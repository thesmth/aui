'use strict';

import isClipped from '../../../src/js/aui/is-clipped';

describe('aui/is-clipped', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            isClipped: isClipped
        });
    });

    describe('API', function () {
        beforeEach(function () {
            $('#test-fixture').html(
                '<p id="shouldBeClipped">Long Long Long Long Long Long Long Long Long Long Long Long Long Long Long</p>' +
                '<p id="shouldNotBeClipped">Short</p>'
            );
        });

        it('long paragraph', function () {
            expect(isClipped($('#shouldBeClipped'))).to.be.true;
        });

        it('short paragraph', function () {
            expect(isClipped($('#shouldNotBeClipped'))).to.be.false;
        });
    });
});
