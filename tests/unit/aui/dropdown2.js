'use strict';

import '../../../src/js/aui/dropdown2';
import * as browser from '../../../src/js/aui/internal/browser';
import AccessibleDropdown from './dropdown2/dropdown2-test-accessible-helper';
import aui from '../../../src/js/aui-soy';
import helpers from '../../helpers/all';
import keyCode from '../../../src/js/aui/key-code';
import LegacyDropdown from './dropdown2/dropdown2-test-legacy-helper';
import skate from 'skatejs';
import uniqueId from '../../../src/js/aui/unique-id';
import WebComponentDropdown from './dropdown2/dropdown2-test-webcomponent-helper';

describe.only('Dropdown2', function () {
    function pressKey (keyCode, modifiers) {
        var e = $.Event('keydown');
        modifiers = modifiers || {};
        e.keyCode = keyCode;
        e.ctrlKey = !!modifiers.control;
        e.shiftKey = !!modifiers.shift;
        e.altKey = !!modifiers.alt;
        e.metaKey = !!modifiers.meta;
        $(document.activeElement).trigger(e);
    }

    function click (element) {
        $(element).click();
    }

    function hover (element) {
        var hoverEvents = ['mouseenter','mouseover','mousemove'];
        $.each(hoverEvents, function(i, name) {
            var e = $.Event(name);
            $(element).trigger(e);
        });
    }

    function isVisible (element) {
        var $element = $(element);
        return $element.is(':visible');
    }

    function hasActiveClasses (element) {
        var $anchor = $(element).find('a').andSelf();
        return $anchor.is('.active.aui-dropdown2-active');
    }

    // Dropdown2 - Construction
    // ------------------------
    //
    // Test the construction of dropdowns.
    // Checking basic state, and looking for race conditions or order of operations problems.
    //
    describe('Construction -', function () {
        var $trigger;
        var $dropdown;

        beforeEach(function () {
            $trigger = $('<button data-aui-trigger="toggle" aria-controls="dd22" ></button>');
            $dropdown = $('<div id="dd22" data-aui-dropdown2=""></div>');
        });

        afterEach(function () {
            helpers.removeLayers();
        });

        it('via data-aui-dropdown2 attribute', function() {
            skate.init($dropdown);
            expect($dropdown.attr('resolved')).to.equal('');
            expect($dropdown.is('.aui-dropdown2')).to.be.true;
        });

        it('via aui-dropdown2 class', function() {
            $dropdown.removeAttr('data-aui-dropdown2');
            $dropdown.addClass('aui-dropdown2');
            skate.init($dropdown);
            expect($dropdown.attr('resolved')).to.equal('');
        });

        it('hides dropdown upon construction', function() {
            skate.init($dropdown);
            expect(isVisible($dropdown)).to.be.false;
            expect($dropdown.attr('aria-hidden')).to.equal('true');
        });
    });

    describe('lazy construction', function () {
        var clock;
        var $trigger;
        var $dropdown;

        function click($el) {
            $el.trigger('mousedown');
            $el.click();
            $el.trigger('mouseup');
        }

        beforeEach(function () {
            clock = sinon.useFakeTimers();
            $trigger = $('<a></a>');
            $dropdown = $('<div id="lazy-dropdown" data-aui-dropdown2=""></div>');

            AJS.$('#test-fixture').append($trigger);
            AJS.$('#test-fixture').append($dropdown);
            skate.init($dropdown); //only skate.init $dropdown and not $trigger as we're testing the synchronous initialisation of the trigger

        });

        afterEach(function () {
            helpers.removeLayers();
            clock = sinon.restore();
        });

        it('trigger is not exapndable before attributes are added', function () {
            click($trigger);
            expect($trigger.attr('aria-expanded')).to.not.equal('true');
        });

        it('dropdown is not visible before attributes are added', function () {
            click($trigger);
            expect($dropdown.is(':visible')).to.be.false;
        });

        describe('after attributes are lazily added', function () {
            beforeEach(function () {
                $trigger.addClass('aui-dropdown2-trigger');
                $trigger.attr('aria-owns', 'lazy-dropdown');
                $trigger.attr('data-aui-trigger', 'toggle');
            });

            describe('followed by clicking on the trigger', function () {
                beforeEach(function() {
                    click($trigger);
                });

                it('expands the trigger', function () {
                    expect($trigger.attr('aria-expanded')).to.equal('true');
                });

                it('makes the dropdown visible', function () {
                    expect($dropdown.is(':visible')).to.be.true;
                });

                it('puts functions on the trigger element', function () {
                    expect($trigger[0].isEnabled).to.be.a('function');
                });
            });
        });
    });

    describe('with voiceover', function () {
        beforeEach(function () {
            sinon.stub(browser, 'supportsVoiceOver').returns(true);
        });

        afterEach(function () {
            browser.supportsVoiceOver.restore();
        });

        runSharedTests();
    });

    describe('without voiceover', function () {
        beforeEach(function () {
            sinon.stub(browser, 'supportsVoiceOver').returns(false);
        });

        afterEach(function () {
            browser.supportsVoiceOver.restore();
        });

        runSharedTests();
    });


    function runSharedTests () {
        describe('with legacy markup pattern', function () {
            runDropdown2UnitTests(LegacyDropdown);
        });

        describe('with new, accessible markup pattern', function () {
            runDropdown2UnitTests(AccessibleDropdown);
        });

        describe('with the web component markup pattern', function () {
            runDropdown2UnitTests(WebComponentDropdown);
        });
    }


    function runDropdown2UnitTests(Dropdown) {
        // Dropdown2 - Single Dropdown Environment
        // ---------------------------------------
        //
        // Create the basic dropdown test environment.
        // There is only one dropdown and one trigger in this environment.
        // The assertion of how dropdowns behave with other dropdowns and layers comes later.
        //
        describe('and one dropdown+trigger', function () {
            var clock;
            var $hideout;
            var singleDropdown;

            beforeEach(function () {
                clock = sinon.useFakeTimers();

                singleDropdown = new Dropdown();
                singleDropdown.addTrigger();

                $hideout = $('<div id="hideout"></div>');
                AJS.$('#test-fixture').append($hideout);
            });

            afterEach(function () {
                clock.restore();

                $('.aui-dropdown2').remove();
                $('.aui-dropdown2-trigger').remove();
                $hideout.remove();

                singleDropdown = null;

                helpers.removeLayers();
            });

            function invokeTrigger($el) {
                $el.trigger('aui-button-invoke');
                clock.tick(100);
            }

            describe('with a link section,', function () {
                beforeEach(function() {
                    singleDropdown.addPlainSection();
                    singleDropdown.initialise();
                });

                // Dropdown2 - Trigger
                // -------------------
                //
                // Test opening and closing a dropdown via its trigger,
                // and the expected state of both the trigger and dropdown.
                //
                describe('on the trigger element', function () {
                    beforeEach(function () {
                        expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.false;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('false');
                    });

                    it('firing aui-button-invoke event opens dropdown', function () {
                        invokeTrigger(singleDropdown.$trigger);

                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                    });

                    it('firing aui-button-invoke hides dropdown if dropdown was open', function () {
                        invokeTrigger(singleDropdown.$trigger);
                        invokeTrigger(singleDropdown.$trigger);

                        expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.false;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('false');
                    });

                    it('pressing Spacebar opens the dropdown', function () {
                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.SPACE);

                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                    });

                    it('pressing Enter opens the dropdown', function () {
                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.ENTER);

                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                    });

                    it('pressing Up Arrow key opens the dropdown', function () {
                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.UP);

                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                    });

                    it('pressing Down Arrow key opens the dropdown', function () {
                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.DOWN);

                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                    });

                    it('clicking opens the dropdown', function () {
                        click(singleDropdown.$trigger);

                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('true');
                    });

                    it('cannot open dropdown when disabled', function () {
                        singleDropdown.$trigger.attr('aria-disabled', 'true');

                        click(singleDropdown.$trigger);

                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.false;
                        expect(singleDropdown.$trigger.attr('aria-expanded')).to.equal('false');
                        expect(document.location.hash).to.not.equal(singleDropdown.$trigger.attr('href'));
                    });

                    it('preserves existing data-aui-alignment attributes with static alignment', function () {
                        var dropdownEl = singleDropdown.$dropdown[0];
                        dropdownEl.setAttribute('data-aui-alignment', 'something useless');
                        dropdownEl.setAttribute('data-aui-alignment-static', true);

                        click(singleDropdown.$trigger);

                        expect(dropdownEl.getAttribute('data-aui-alignment')).to.equal('something useless');
                        expect(dropdownEl.hasAttribute('data-aui-alignment-static')).to.equal(true);
                    });

                    it('preserves existing data-aui-alignment attributes without static alignment', function () {
                        var dropdownEl = singleDropdown.$dropdown[0];
                        dropdownEl.setAttribute('data-aui-alignment', 'something useless');

                        click(singleDropdown.$trigger);

                        expect(dropdownEl.getAttribute('data-aui-alignment')).to.equal('something useless');
                        expect(dropdownEl.hasAttribute('data-aui-alignment-static')).to.equal(false);
                    });

                    it('clicking adds aui-dropdown2-active and active classes to trigger', function () {
                        click(singleDropdown.$trigger);

                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                    });

                    it('aui-button-invoke adds aui-dropdown2-active and active classes to trigger', function () {
                        invokeTrigger(singleDropdown.$trigger);

                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                    });
                });

                describe('on the dropdown element', function () {
                    afterEach(function () {
                        singleDropdown.$dropdown.off('aui-dropdown2-show');
                        singleDropdown.$dropdown.off('aui-dropdown2-hide');
                    });

                    it('aui-dropdown2-show event is fired when it is shown by click', function () {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-show', spy);

                        click(singleDropdown.$trigger);

                        spy.should.have.been.calledOnce;
                    });

                    it('aui-dropdown2-show event is fired when it is shown by invoke', function () {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-show', spy);

                        invokeTrigger(singleDropdown.$trigger);

                        spy.should.have.been.calledOnce;
                    });

                    it('aui-dropdown2-hide is fired when it is hidden by click', function () {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-hide', spy);

                        clock.tick(1000);
                        click(singleDropdown.$trigger);
                        clock.tick(1000);
                        click(singleDropdown.$trigger);
                        clock.tick(1000);

                        spy.should.have.been.calledOnce;
                    });

                    it('aui-dropdown2-hide is fired when it is closed by invoke', function () {
                        var spy = sinon.spy();
                        singleDropdown.$dropdown.on('aui-dropdown2-hide', function () {
                            spy();
                        });

                        click(singleDropdown.$trigger);
                        invokeTrigger(singleDropdown.$trigger);

                        spy.should.have.been.calledOnce;
                    });

                    it('aria-hidden set to false when opened', function () {
                        click(singleDropdown.$trigger);
                        expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('false');
                    });

                    it('aria-hidden set to true when hidden', function () {
                        click(singleDropdown.$trigger);
                        click(singleDropdown.$trigger);
                        expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('true');
                    });

                    it('closes when Escape is pressed', function () {
                        click(singleDropdown.$trigger);
                        pressKey(AJS.keyCode.ESCAPE);
                        expect(singleDropdown.$dropdown.attr('aria-hidden')).to.equal('true');
                    });
                });

                // Dropdown2 - API
                // ---------------
                //
                // Test the functions present on a dropdown2 component element
                //
                describe('Dropdown Element API -', function () {
                    it('show() will open dropdown', function () {
                        singleDropdown.$dropdown[0].show();
                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    });

                    it('hide() will close dropdown', function () {
                        singleDropdown.$dropdown[0].show();
                        singleDropdown.$dropdown[0].hide();
                        expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                    });

                    it('isVisible() returns false when hidden on load', function () {
                        expect(singleDropdown.$dropdown[0].isVisible()).to.be.false;
                    });

                    it('isVisible() returns true when visible', function () {
                        singleDropdown.$dropdown[0].show();
                        expect(singleDropdown.$dropdown[0].isVisible()).to.be.true;
                    });

                    it('isVisible() returns false when hidden after hide', function () {
                        singleDropdown.$dropdown[0].hide();
                        expect(singleDropdown.$dropdown[0].isVisible()).to.be.false;
                    });
                });

                // Dropdown 2 - DOM
                // ----------------
                //
                // Test configuring a dropdown element's resting place, etc.
                //
                describe('DOM -', function () {
                    it('returned to original location if data-dropdown2-hide-location is not specified', function () {
                        var originalParent = singleDropdown.$dropdown.parent()[0];

                        click(singleDropdown.$trigger);
                        clock.tick(100);
                        click(singleDropdown.$trigger);
                        clock.tick(100);

                        expect(singleDropdown.$dropdown.parent()[0]).to.equal(originalParent);
                        expect($hideout.find(singleDropdown.$dropdown).length).to.equal(0);
                    });

                    it('specifying the data-dropdown2-hide-location works properly when dropdown is hidden', function () {
                        singleDropdown.$trigger.attr('data-dropdown2-hide-location', 'hideout');
                        var originalParent = singleDropdown.$dropdown.parent()[0];

                        click(singleDropdown.$trigger);
                        clock.tick(100);
                        click(singleDropdown.$trigger);
                        clock.tick(100);

                        expect(singleDropdown.$dropdown.parent()[0]).to.not.equal(originalParent);
                        expect($hideout.find(singleDropdown.$dropdown).length).to.equal(1);
                    });
                });

                // Dropdown2 - Items
                // -----------------
                //
                // Test navigating and interacting with items in the dropdown
                //
                describe('Items -', function () {
                    beforeEach(function() {
                        singleDropdown.addPlainSection();
                        singleDropdown.addHiddenSection();
                        singleDropdown.addInteractiveSection();
                        singleDropdown.initialise();
                    });

                    it('first item NOT active when dropdown opened by mouse', function() {
                        var $i1 = singleDropdown.getItem(1);

                        click(singleDropdown.$trigger);

                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(hasActiveClasses($i1)).to.be.false;
                    });

                    it('first item is focused when dropdown opened by keyboard (a11y)', function() {
                        var $i1 = singleDropdown.getItem(1);

                        singleDropdown.$trigger.focus();
                        expect(document.activeElement).to.equal(singleDropdown.$trigger[0]);

                        pressKey(AJS.keyCode.SPACE);
                        expect(hasActiveClasses($i1)).to.be.true;
                    });

                    it('can navigate items using down arrow key', function() {
                        var $i1 = singleDropdown.getItem(1);
                        var $i2 = singleDropdown.getItem(2);

                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        pressKey(AJS.keyCode.DOWN);

                        expect(hasActiveClasses($i1)).to.be.false;
                        expect(hasActiveClasses($i2)).to.be.true;
                    });

                    it('navigated correctly using keys with hidden items', function() {
                        var $i1 = singleDropdown.getItem(1),
                            $i2 = singleDropdown.getItem(2),
                            $i3 = singleDropdown.getItem(3),
                            $i4 = singleDropdown.getItem(4);

                        $i1.parent().addClass('hidden');
                        $i3.parent().addClass('hidden');

                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        expect(hasActiveClasses($i1)).to.be.false;
                        expect(hasActiveClasses($i2)).to.be.true;

                        pressKey(AJS.keyCode.DOWN);

                        expect(hasActiveClasses($i2)).to.be.false;
                        expect(hasActiveClasses($i3)).to.be.false;
                        expect(hasActiveClasses($i4)).to.be.true;
                    });

                    it('navigated correctly using keys with hidden items that were added after opened', function() {
                        var $i1 = singleDropdown.getItem(1),
                            $i2 = singleDropdown.getItem(2),
                            $i3 = singleDropdown.getItem(3),
                            $i4 = singleDropdown.getItem(4);

                        $i1.parent().addClass('hidden');

                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.ENTER);

                        $i3.parent().addClass('hidden');
                        pressKey(AJS.keyCode.DOWN);

                        expect(hasActiveClasses($i2)).to.be.false;
                        expect(hasActiveClasses($i3)).to.be.false;
                        expect(hasActiveClasses($i4)).to.be.true;
                    });

                    it('can be focused with the keyboard even when disabled (a11y)', function() {
                        var $i2 = singleDropdown.getItem(2);
                        var $i3 = singleDropdown.getItem(3);
                        var $i4 = singleDropdown.getItem(4);

                        $i2.attr('aria-disabled', true);
                        $i3.attr('aria-disabled', true);

                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        pressKey(AJS.keyCode.DOWN);

                        expect(hasActiveClasses($i4)).to.be.false;
                        expect(hasActiveClasses($i2)).to.be.true;
                    });

                    it('can be activated by clicking', function() {
                        var spy = sinon.spy();
                        var $i1 = singleDropdown.getItem(1).attr('href','#actionable');

                        $i1.on('click', spy);
                        click(singleDropdown.$trigger);
                        click($i1);

                        spy.should.have.been.calledOnce;
                        expect(spy.getCall(0).args[0].isDefaultPrevented()).to.be.false;
                    });

                    it('clicking on links works', function () {
                        click(singleDropdown.$trigger);
                        singleDropdown.getItem(1)[0].click();
                        expect(window.location.hash).to.equal('#link');
                    });

                    it('cannot activate disabled items', function() {
                        var spy = sinon.spy();
                        var $i1 = singleDropdown.getItem(1).attr('href', '#shouldnt-be-actionable');
                        $i1.attr('aria-disabled', true);

                        $i1.on('click', spy);
                        click(singleDropdown.$trigger);
                        click($i1);

                        spy.should.have.been.calledOnce;
                        expect(spy.getCall(0).args[0].isDefaultPrevented()).to.be.true;
                    });

                    it('clicking normal dropdown items should close dropdown', function () {
                        var $ir3 = singleDropdown.getItem(3, 3);

                        click(singleDropdown.$trigger);

                        click($ir3);
                        clock.tick(100);

                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.false;
                        expect(isVisible(singleDropdown.$dropdown)).to.be.false;
                    });

                    it('clicking a dropdown item with the \'aui-dropdown2-interactive\' class doesn\'t hide the dropdown', function() {
                        var $ir1 = singleDropdown.getItem(1, 3);

                        click(singleDropdown.$trigger);

                        click($ir1);
                        clock.tick(100);

                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    });

                    it('clicking a dropdown item with the legacy \'interactive\' class doesn\'t hide the dropdown', function() {
                        var $ir2 = singleDropdown.getItem(3, 2);

                        click(singleDropdown.$trigger);

                        click($ir2);
                        clock.tick(100);

                        expect(hasActiveClasses(singleDropdown.$trigger)).to.be.true;
                        expect(isVisible(singleDropdown.$dropdown)).to.be.true;
                    });

                    it('are reverted to inactive when the dropdown is closed', function() {
                        var $i3 = singleDropdown.getItem(3);

                        singleDropdown.$trigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        pressKey(AJS.keyCode.DOWN);
                        pressKey(AJS.keyCode.DOWN);

                        expect(hasActiveClasses($i3)).to.be.true;

                        pressKey(AJS.keyCode.ESCAPE);

                        expect(hasActiveClasses($i3)).to.be.false;
                    });
                });
            });

            describe('with a trigger on the left side of the window that has been clicked', function () {
                beforeEach(function () {
                    singleDropdown.$trigger.css({
                        float: 'left'
                    });
                    singleDropdown.initialise();
                    click(singleDropdown.$trigger);
                });

                afterEach(function () {
                    singleDropdown.$dropdown.removeAttr('style');
                });

                it('opens a dropdown that is anchored to the left hand side of the trigger', function () {
                    var dropdownLeft = (singleDropdown.$dropdown.position().left);
                    var triggerLeft = (singleDropdown.$trigger.position().left);
                    expect(triggerLeft).to.be.within(dropdownLeft-1, dropdownLeft);
                });
            });

            describe('with a trigger on the right side of the window that has been clicked', function () {
                beforeEach(function () {
                    singleDropdown.$trigger.css({
                        float: 'right'
                    });
                    singleDropdown.initialise();
                    click(singleDropdown.$trigger);
                });

                afterEach(function () {
                    singleDropdown.$dropdown.removeAttr('style');
                });

                it('opens a dropdown that is anchored to the right hand side of the trigger', function () {

                    var dropdownRight = singleDropdown.$dropdown.position().left + singleDropdown.$dropdown.outerWidth();
                    var triggerRight = singleDropdown.$trigger.position().left + singleDropdown.$trigger.outerWidth();
                    expect(triggerRight).to.be.within(dropdownRight, dropdownRight+1);
                });
            });

            // Dropdown2 - Special Items
            // -------------------------
            //
            // Test interaction with special dropdown item types, like
            // checkboxes and radio buttons.
            //
            describe('with checkbox and hidden items; clicking the trigger', function () {
                beforeEach(function() {
                    singleDropdown.addCheckboxSection();
                    singleDropdown.addHiddenSection();
                    singleDropdown.initialise();

                    click(singleDropdown.$trigger);
                });

                it('adds aui-dropdown2-checked on click checkboxes', function() {
                    var $c1 = singleDropdown.getItem(1);
                    click($c1);
                    expect($c1.is('.checked.aui-dropdown2-checked')).to.be.true;
                });

                it('toggles checked to unchecked on checkboxes', function() {
                    var $c1 = singleDropdown.getItem(1);
                    var $c2 = singleDropdown.getItem(2);

                    click($c1);
                    click($c2);

                    expect($c1.is('.checked.aui-dropdown2-checked')).to.be.true;
                    expect($c2.is('.checked,.aui-dropdown2-checked')).to.be.false;
                });

                it('fires aui-dropdown2-item-check on checkboxes', function() {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-item-check', spy);

                    var $c1 = singleDropdown.getItem(1),
                        $c2 = singleDropdown.getItem(2),
                        $c3 = singleDropdown.getItem(3);

                    click($c1);
                    click($c2);
                    click($c3);

                    spy.should.have.been.calledTwice;
                });

                it('fires aui-dropdown2-item-uncheck on checkboxes', function() {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-item-uncheck', spy);

                    var $c1 = singleDropdown.getItem(1),
                        $c2 = singleDropdown.getItem(2),
                        $c3 = singleDropdown.getItem(3);

                    click($c1);
                    click($c2);
                    click($c3);

                    spy.should.have.been.calledOnce;
                });

                it('cannot check hidden, disabled checkboxes', function() {
                    var $h1 = singleDropdown.getItem(1, 2);
                    var $h2 = singleDropdown.getItem(2, 2);

                    click($h1);
                    click($h2);

                    expect($h1.is('.checked.aui-dropdown2-checked')).to.be.false;
                    expect($h2.is('.aui-dropdown2-checked')).to.be.true;
                });

                it('and pressing enter on a checkbox item toggles it', function () {
                    var $c1 = singleDropdown.getItem(1);
                    helpers.pressKey('enter', {}, $c1[0]);
                    expect($c1.is('.checked.aui-dropdown2-checked')).to.be.true;
                });

                it('and pressing space on a checkbox item toggles it', function () {
                    var $c1 = singleDropdown.getItem(1);
                    helpers.pressKey('space', {}, $c1[0]);
                    expect($c1.is('.checked.aui-dropdown2-checked')).to.be.true;
                });
            });

            describe('with radio buttons; clicking the trigger', function () {
                beforeEach(function() {
                    singleDropdown.addRadioSection();
                    singleDropdown.initialise();
                    click(singleDropdown.$trigger);
                });

                it('and clicking radio buttons adds aui-dropdown2-checked', function() {
                    var $r3 = singleDropdown.getItem(3);
                    click($r3);
                    expect($r3.is('.checked.aui-dropdown2-checked')).to.be.true;
                });

                it('toggles checked to unchecked on radio buttons', function() {
                    var $r1 = singleDropdown.getItem(1);
                    var $r2 = singleDropdown.getItem(2);

                    click($r1);

                    expect($r1.is('.checked.aui-dropdown2-checked')).to.be.true;
                    expect($r2.is('.checked,.aui-dropdown2-checked')).to.be.false;
                });

                it('fires aui-dropdown2-item-check on radios', function() {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-item-check', spy);

                    var $r1 = singleDropdown.getItem(1);
                    var $r2 = singleDropdown.getItem(2);
                    var $r3 = singleDropdown.getItem(3);

                    click($r2);
                    click($r1);
                    click($r3);

                    spy.should.have.been.calledTwice;
                });

                it('fires aui-dropdown2-item-uncheck on radios', function() {
                    var spy = sinon.spy();
                    singleDropdown.$dropdown.on('aui-dropdown2-item-check', spy);

                    var $r1 = singleDropdown.getItem(1);
                    var $r2 = singleDropdown.getItem(2);
                    var $r3 = singleDropdown.getItem(3);

                    click($r1);
                    click($r2);
                    click($r3);

                    spy.should.have.been.calledThrice;
                });

                it('and pressing enter on a radio item toggles it', function () {
                    var $r3 = singleDropdown.getItem(3);
                    helpers.pressKey('enter', {}, $r3[0]);
                    expect($r3.is('.checked.aui-dropdown2-checked')).to.be.true;
                });

                it('and pressing space on a radio item toggles it', function () {
                    var $r3 = singleDropdown.getItem(3);
                    helpers.pressKey('space', {}, $r3[0]);
                    expect($r3.is('.checked.aui-dropdown2-checked')).to.be.true;
                });
            });

            // Dropdown2 - Submenus
            // --------------------
            //
            // Test environment setup for dropdowns with submenus.
            // Test the basic interactions with dropdown submenus.
            //
            describe('which has a submenu', function () {
                var clock;
                var $firstMenuTrigger;
                var $secondMenuTrigger;
                var $thirdMenuTrigger;
                var firstSubmenu;
                var secondSubmenu;

                beforeEach(function () {
                    firstSubmenu = new Dropdown();
                    singleDropdown.addSubmenuSection(firstSubmenu);

                    secondSubmenu = new Dropdown();
                    firstSubmenu.addSubmenuSection(secondSubmenu);

                    secondSubmenu.addPlainSection();

                    singleDropdown.initialise();
                    firstSubmenu.initialise();
                    secondSubmenu.initialise();

                    clock = sinon.useFakeTimers();

                    $firstMenuTrigger = singleDropdown.$trigger;
                    $secondMenuTrigger = singleDropdown.getItem(2);
                    $thirdMenuTrigger = firstSubmenu.getItem(2);
                });

                afterEach(function () {
                    clock.restore();
                    helpers.removeLayers();
                });

                function countOpenDropdowns () {
                    return $('.aui-dropdown2[aria-hidden=false]').length;
                }

                it('can be opened with aui-button-invoke', function() {
                    expect(countOpenDropdowns()).to.equal(0);

                    invokeTrigger($firstMenuTrigger);
                    invokeTrigger($secondMenuTrigger);

                    expect(countOpenDropdowns()).to.equal(2);
                });

                it('will focus first item in submenu if opened via keyboard (a11y)', function() {
                    $firstMenuTrigger.focus();
                    clock.tick(100);
                    pressKey(AJS.keyCode.SPACE);
                    clock.tick(100);
                    $secondMenuTrigger.focus();
                    clock.tick(100);
                    pressKey(AJS.keyCode.SPACE);
                    clock.tick(100);

                    var $firstMenuFirstItem = firstSubmenu.getItem(1);
                    expect(document.activeElement).to.equal($firstMenuFirstItem[0]);
                });

                it('can be opened via the right arrow key', function() {
                    $firstMenuTrigger.focus();
                    pressKey(AJS.keyCode.ENTER);
                    $secondMenuTrigger.focus();
                    pressKey(AJS.keyCode.RIGHT);
                    expect(countOpenDropdowns()).to.equal(2);
                    pressKey(AJS.keyCode.DOWN);
                    pressKey(AJS.keyCode.RIGHT);
                    expect(countOpenDropdowns()).to.equal(3);
                });

                it('will not open when pressing right arrow on a non trigger item', function() {
                    $firstMenuTrigger.focus();
                    pressKey(AJS.keyCode.ENTER);
                    pressKey(AJS.keyCode.RIGHT);
                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('will not open when pressing the up arrow on a trigger item', function() {
                    $firstMenuTrigger.focus();
                    pressKey(AJS.keyCode.ENTER);
                    $secondMenuTrigger.focus();
                    pressKey(AJS.keyCode.UP);
                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('will not open when pressing the down arrow on a trigger item', function() {
                    $firstMenuTrigger.focus();
                    pressKey(AJS.keyCode.ENTER);
                    $secondMenuTrigger.focus();
                    pressKey(AJS.keyCode.DOWN);
                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('can be opened by hovering their trigger', function () {
                    var $nonTriggerInFirstMenu = singleDropdown.getItem(1);

                    click($firstMenuTrigger);
                    hover($secondMenuTrigger);
                    clock.tick(100);
                    hover($thirdMenuTrigger);
                    clock.tick(100);

                    expect(countOpenDropdowns()).to.equal(3);

                    hover($nonTriggerInFirstMenu);
                    clock.tick(100);

                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('cannot be opened if their trigger is aria-disabled', function() {
                    $secondMenuTrigger.attr('aria-disabled', 'true');

                    click($firstMenuTrigger);

                    hover($secondMenuTrigger);
                    expect(countOpenDropdowns()).to.equal(1);

                    click($secondMenuTrigger);
                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('cannot be opened if their triggers have the disabled or aui-dropdown2-disabled class', function() {
                    $secondMenuTrigger.addClass('disabled aui-dropdown2-disabled');

                    click($firstMenuTrigger);

                    hover($secondMenuTrigger);
                    expect(countOpenDropdowns()).to.equal(1);

                    click($secondMenuTrigger);
                    expect(countOpenDropdowns()).to.equal(1);
                });

                it('will not close top-level dropdown when pressing left arrow key in a top-level menu', function() {
                    $firstMenuTrigger.focus();
                    pressKey(AJS.keyCode.ENTER);
                    pressKey(AJS.keyCode.LEFT);

                    expect(countOpenDropdowns()).to.be.at.least(1);
                });

                describe('and all submenus are opened with an event', function () {
                    beforeEach(function () {
                        invokeTrigger($firstMenuTrigger);
                        invokeTrigger($secondMenuTrigger);
                        invokeTrigger($thirdMenuTrigger);
                        clock.tick(100);
                    });

                    it('leave three open dropdowns', function () {
                        expect(countOpenDropdowns()).to.equal(3);
                    });

                    it('will close all nested submenus when the submenu trigger is clicked', function() {
                        click($secondMenuTrigger);
                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('will not place focus on dropdown trigger when clicking the document to close dropdowns', function() {
                        click(document);
                        clock.tick(100);
                        expect(document.activeElement).to.not.equal($firstMenuTrigger[0]);
                    });
                });

                describe('and all submenus are opened with click', function () {
                    beforeEach(function () {
                        click($firstMenuTrigger);
                        click($secondMenuTrigger);
                        click($thirdMenuTrigger);
                    });

                    it('leave three open dropdowns', function () {
                        expect(countOpenDropdowns()).to.equal(3);
                    });

                    it('will all close when the document is clicked', function() {
                        click(document);
                        clock.tick(100);
                        expect(countOpenDropdowns()).to.equal(0);
                    });

                    it('will all close when root dropdown trigger is clicked', function() {
                        click($firstMenuTrigger);
                        expect(countOpenDropdowns()).to.equal(0);
                    });
                });

                describe('and all submenus are opened with hover', function () {
                    beforeEach(function () {
                        click($firstMenuTrigger);
                        hover($secondMenuTrigger);
                        clock.tick(100);
                        hover($thirdMenuTrigger);
                        clock.tick(100);
                    });

                    it('leave three open dropdowns', function () {
                        expect(countOpenDropdowns()).to.equal(3);
                    });

                    it('will not place focus on dropdown submenu trigger when hovering a non-trigger item in that menu', function() {
                        var $nonTriggerInFirstMenu = singleDropdown.getItem(1);
                        hover($nonTriggerInFirstMenu);
                        clock.tick(100);

                        expect(countOpenDropdowns()).to.equal(1);
                        expect(document.activeElement).to.not.equal($firstMenuTrigger[0]);
                    });
                });

                describe('and all submenus are opened with the keyboard', function () {
                    beforeEach(function () {
                        $firstMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        $secondMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                        $thirdMenuTrigger.focus();
                        pressKey(AJS.keyCode.ENTER);
                    });

                    it('leave three open dropdowns', function () {
                        expect(countOpenDropdowns()).to.equal(3);
                    });

                    it('then dropdowns can be closed from any position in a dropdown with the left arrow key', function() {

                        pressKey(AJS.keyCode.LEFT);

                        expect(countOpenDropdowns()).to.equal(2);

                        pressKey(AJS.keyCode.UP);
                        pressKey(AJS.keyCode.LEFT);

                        expect(countOpenDropdowns()).to.equal(1);
                    });

                    it('will leave their triggers active when closed by pressing Escape', function() {
                        expect(hasActiveClasses($secondMenuTrigger)).to.be.true;
                        expect(hasActiveClasses($thirdMenuTrigger)).to.be.true;

                        pressKey(AJS.keyCode.ESCAPE);

                        expect(hasActiveClasses($thirdMenuTrigger)).to.be.true;
                        expect(hasActiveClasses($secondMenuTrigger)).to.be.true;
                        expect(document.activeElement).to.equal($thirdMenuTrigger[0]);

                        pressKey(AJS.keyCode.ESCAPE);

                        expect(hasActiveClasses($thirdMenuTrigger)).to.be.false;
                        expect(hasActiveClasses($secondMenuTrigger)).to.be.true;
                        expect(document.activeElement).to.equal($secondMenuTrigger[0]);
                    });

                    it('will close one submenu at a time when pressing Escape', function() {
                        expect(countOpenDropdowns()).to.equal(3);
                        pressKey(AJS.keyCode.ESCAPE);
                        expect(countOpenDropdowns()).to.equal(2);
                        pressKey(AJS.keyCode.ESCAPE);
                        expect(countOpenDropdowns()).to.equal(1);
                        pressKey(AJS.keyCode.ESCAPE);
                        expect(countOpenDropdowns()).to.equal(0);
                    });

                    it('will place focus on dropdown trigger when closed with Escape', function() {
                        pressKey(AJS.keyCode.ESCAPE);
                        expect(document.activeElement).to.equal($thirdMenuTrigger[0]);
                        pressKey(AJS.keyCode.ESCAPE);
                        expect(document.activeElement).to.equal($secondMenuTrigger[0]);
                        pressKey(AJS.keyCode.ESCAPE);
                        expect(document.activeElement).to.equal($firstMenuTrigger[0]);
                    });
                });
            });
        });

        // Dropdown2 - Multiple Dropdown Environment
        // -----------------------------------------
        //
        // Create a dropdown test environment with multiple dropdowns.
        // This environment is for asserting that interaction a second dropdown+trigger will
        // put the first dropdown+trigger in an expected state.
        //
        describe('and multiple dropdowns', function () {
            var clock;
            var dropdown1;
            var dropdown2;

            beforeEach(function () {
                clock = sinon.useFakeTimers();

                dropdown1 = new Dropdown();
                dropdown1.addPlainSection();
                dropdown1.addPlainSection2();
                dropdown1.addTrigger();

                dropdown2 = new Dropdown();
                dropdown2.addPlainSection();
                dropdown2.addPlainSection2();
                dropdown2.addTrigger();

                var $triggerGroup = AJS.$('<ul class="aui-dropdown2-trigger-group"></ul>');
                AJS.$('#test-fixture').append($triggerGroup);

                dropdown1.initialise($triggerGroup);
                $triggerGroup.append('<a href="#">Not a menu trigger</a>');
                dropdown2.initialise($triggerGroup);
            });

            afterEach(function () {
                clock.restore();

                $('.aui-dropdown2').remove();
                $('.aui-dropdown2-trigger').remove();

                dropdown1.$dropdown.remove();
                dropdown1.$trigger.remove();
                dropdown2.$dropdown.remove();
                dropdown2.$trigger.remove();

                dropdown1 = null;
                dropdown2 = null;

                helpers.removeLayers();
            });

            it('open on click correctly one dropdown', function () {
                click(dropdown1.$trigger);
                clock.tick(100);

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.true;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
            });

            it('in aui-dropdown2-trigger-group open on click correctly multiple dropdowns', function () {
                click(dropdown1.$trigger);
                clock.tick(100);
                click(dropdown2.$trigger);
                clock.tick(100);

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
            });

            it('in aui-dropdown2-trigger-group open on hover after one is clicked', function () {
                click(dropdown1.$trigger);
                clock.tick(100);
                hover(dropdown2.$trigger);
                clock.tick(0);

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
            });

            it('in aui-dropdown2-trigger-group doesnt open on over when one not clicked', function () {
                hover(dropdown2.$trigger);
                clock.tick(0);

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
            });

            it('in aui-dropdown2-trigger-group pressing tab will focus next item in group', function () {
                var keySpy = sinon.spy();

                dropdown1.$trigger.focus();
                pressKey(AJS.keyCode.ENTER);
                pressKey(AJS.keyCode.DOWN);

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.true;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;

                $(document).on('keydown', keySpy);
                pressKey(AJS.keyCode.TAB);
                $(document).off('keydown', keySpy);

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
                // I'd love to test document.activeElement, but I can't adequately fake the native behaviour such that the focus moves of its own accord in a test.
                keySpy.should.have.been.calledOnce;
                expect(keySpy.args[0][0].isDefaultPrevented()).to.be.false;
            });

            it('in aui-dropdown2-trigger-group pressing shift-tab will focus previous item in group', function () {
                var keySpy = sinon.spy();

                dropdown2.$trigger.focus();
                pressKey(AJS.keyCode.ENTER);
                pressKey(AJS.keyCode.DOWN);

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;

                $(document).one('keydown', keySpy);
                pressKey(AJS.keyCode.TAB, { shift: true });

                expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
                expect(hasActiveClasses(dropdown2.$trigger)).to.be.false;
                // I'd love to test document.activeElement, but I can't adequately fake the native behaviour such that the focus moves of its own accord in a test.
                keySpy.should.have.been.calledOnce;
                expect(keySpy.args[0][0].isDefaultPrevented()).to.be.false;
            });

            it('only allows for one trigger to have aui-dropdown2-active and active classes', function () {
                click(dropdown1.$trigger);
                expect(hasActiveClasses(dropdown1.$trigger)).to.be.true;

                // Open the second dropdown
                click(dropdown2.$trigger);

                expect(hasActiveClasses(dropdown2.$trigger)).to.be.true;
                expect(hasActiveClasses(dropdown1.$trigger)).to.be.false;
            });
        });
    }

});
