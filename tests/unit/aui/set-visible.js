'use strict';

import setVisible from '../../../src/js/aui/set-visible';

describe('aui/set-visible', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            setVisible: setVisible
        });
    });
});
