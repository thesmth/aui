'use strict';

import skateTemplateHtml from 'skatejs-template-html';
import aui from '../../../src/js/aui-soy';
import skate from 'skatejs';
import tabs from '../../../src/js/aui/tabs';

describe('aui/tabs', function () {
    var fixture;
    var DATA_TABS_PERSIST = 'data-aui-persist';
    var STORAGE_PREFIX = '_internal-aui-tabs-';

    function getAnchor (id) {
        return document.querySelector('[href$="' + id + '"]');
    }

    function getKey (id) {
        return getAnchor(id).getAttribute('href');
    }

    function select (id) {
        $('aui-tabs').each(function () {
            this.select(document.getElementById(id));
        });
    }

    function persistAttribute (c) {
        return c.persist || c.persist === '' ?
            ' ' + DATA_TABS_PERSIST + '=' + (c.persist ? c.persist : '""') :
            '';
    }

    function renderTemplate (c, init) {
        c = $.extend({
            id: 'aui-tabs',
            persist: undefined,
            tabPostfix: ''
        }, c);

        fixture.innerHTML += '' +
            '<aui-tabs id="' + c.id + '" ' + persistAttribute(c) + '>' +
                '<aui-tabs-pane id="tab-lister' + c.tabPostfix + '" title="Lister">+1</aui-tabs-pane>' +
                '<aui-tabs-pane id="tab-rimmer' + c.tabPostfix + '" title="Rimmer">-1</aui-tabs-pane>' +
            '</aui-tabs>';

        // For the tests that call this function more than once
        // consecutively, this will get called the same number of times
        // and causes the tab list items to disappear from the first
        // tab group. It's probably a race condition with `skate.init()`.
        if (init || init === undefined) {
            skate.init(fixture);
        }
    }

    function addTabPane () {
        var pane = document.createElement('aui-tabs-pane');

        pane.id = 'tab-kryten';
        pane.title = 'Kryten';
        pane.textContent = '+∞';

        skateTemplateHtml.wrap(fixture.children[0]).appendChild(pane);
        skate.init(pane);
    }

    function createPersistentKey ($tabGroup) {
        var tabGroupId = $tabGroup.attr('id');
        var value = $tabGroup.attr(DATA_TABS_PERSIST);

        return STORAGE_PREFIX + (tabGroupId ? tabGroupId : '') + (value && value !== 'true' ? '-' + value : '');
    }

    function getPersistent (groupId) {
        // groupId must exist in the dom
        var key = createPersistentKey($('#' + groupId));
        return window.localStorage.getItem(key);
    }

    function storePersistent (groupId, value) {
        // Before custom elements we were using the anchor id, now we're
        // using the list-item id. We use this to get the anchor id.
        value = getKey(value);

        // groupId must exist in the dom
        var key = createPersistentKey($('#' + groupId));
        window.localStorage.setItem(key, value);
    }

    beforeEach(function () {
        fixture = document.getElementById('test-fixture');
    });

    afterEach(function () {
        window.localStorage.clear();
    });

    it('globals', function () {
        expect(AJS).to.contain({
            tabs: tabs
        });
    });

    it('API', function () {
        expect(tabs).to.be.an('object');
        expect(tabs.setup).to.be.a('function');
        expect(tabs.change).to.be.a('function');
    });

    it('hide active tab and shows new tab on click', function () {
        renderTemplate();
        select('tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
    });

    it('setup() will not double-bind event handlers on multiple calls.', function () {
        renderTemplate();
        tabs.setup();
        tabs.setup();

        var clickSpy = sinon.spy();

        $('.tabs-menu a').bind('tabSelect', clickSpy);
        select('tab-rimmer');

        clickSpy.should.have.been.calledOnce;
    });

    it('adding a tab dynamically should work', function () {
        renderTemplate();
        addTabPane();
        select('tab-kryten');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.false;
        expect($('#tab-kryten').is(':visible')).to.be.true;
    });

    it('soy template exists', function () {
        expect(aui.tabs).to.be.defined;
    });

    it('soy template basic rendering', function () {
        var html = aui.tabs({
            menuItems : [{
                isActive : true,
                url : '#tab1',
                text : 'Tab 1'
            }, {
                url : '#tab2',
                text : 'Tab 2'
            }],
            paneContent : aui.tabPane({
                isActive : true,
                content : 'Tab 1 Content'
            }) + aui.tabPane({
                content : 'Tab 2 Content'
            })
        });

        var $el = $(html).appendTo($('#test-fixture'));
        expect($el.is('.aui-tabs')).to.be.true;
    });

    it('no id persist=true does not save to local storage', function () {
        renderTemplate({
            persist: 'true'
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.be.not.defined;
    });

    it('persist with no value saves to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: ''
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
    });

    it('persist=true saves to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        });

        select('tab-lister');
        select('tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
    });

    it('persist with unique value saves to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'user1'
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
    });

    it('persist=false does not save to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'false'
        });

        select('tab-lister');
        select('tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.not.be.defined;
    });

    it('no persist does not save to local storage', function () {
        renderTemplate({
            id: 'tabgroup1'
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.not.be.defined;
    });

    it('persist=true saves to local storage multiple tab groups', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        }, false);

        renderTemplate({
            id: 'tabgroup2',
            persist: 'true',
            tabPostfix: '2'
        });

        select('tab-lister');
        select('tab-rimmer');
        select('tab-rimmer2');
        select('tab-lister2');

        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
        expect(getPersistent('tabgroup2')).to.equal(getKey('tab-lister2'));
    });

    it('persist=false doesnt load from local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'false'
        });

        storePersistent('tabgroup1', 'tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.true;
        expect($('#tab-rimmer').is(':visible')).to.be.false;
    });

    it('persist=true loads from local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        });

        storePersistent('tabgroup1', 'tab-rimmer');
        tabs.setup();

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
    });

    it('loading from local storage invokes tabSelect handler', function () {
        var tabSelectSpy = sinon.spy();

        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        });

        storePersistent('tabgroup1', 'tab-rimmer');
        $('.tabs-menu a').bind('tabSelect', tabSelectSpy);
        tabs.setup();

        tabSelectSpy.should.have.been.calledOnce;
    });

    it('persist=true loads from local storage with multiple tab groups', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        }, false);

        renderTemplate({
            id: 'tabgroup2',
            persist: 'true',
            tabPostfix: '2'
        });

        storePersistent('tabgroup1', 'tab-rimmer');
        storePersistent('tabgroup2', 'tab-rimmer2');
        tabs.setup();

        expect($('#tabgroup1').find('#tab-lister').is(':visible')).to.be.false;
        expect($('#tabgroup1').find('#tab-rimmer').is(':visible')).to.be.true;
        expect($('#tabgroup2').find('#tab-lister2').is(':visible')).to.be.false;
        expect($('#tabgroup2').find('#tab-rimmer2').is(':visible')).to.be.true;
    });

    describe('responsive', function () {
        var fixture;
        var responsiveTabs;
        var responsiveTabsHtml = '' +
            '<aui-tabs responsive="true">' +
                '<aui-tabs-pane id="horizontal-wrap-first-2" title="Tab - 1 Active">' +
                    '<h2>This is Tab 1</h2>' +
                    '<p>First</p>' +
                '</aui-tabs-pane>'+
                '<aui-tabs-pane id="horizontal-wrap-second-2" title="Tab 2">' +
                    '<h2>This is Tab 2</h2>' +
                    '<p>Second</p>' +
                '</aui-tabs-pane>'+
                '<aui-tabs-pane id="horizontal-wrap-third-2" title="Tab 3">' +
                    '<h2>This is Tab 3</h2>' +
                    '<p>Third</p>' +
                '</aui-tabs-pane>'+
                '<aui-tabs-pane id="horizontal-wrap-fourth-2" title="Tab 4">' +
                    '<h2>This is Tab 4</h2>' +
                    '<p>Fourth</p>' +
                '</aui-tabs-pane>' +
            '</aui-tabs>';

        beforeEach(function () {
            fixture = document.getElementById('test-fixture');
            fixture.innerHTML = responsiveTabsHtml;
            responsiveTabs = fixture.children[0];
            skate.init(fixture);
        });

        it('should use the immediate container width to calculate which tabs to show', function () {
            var tabs = $('.tabs-menu').find('.menu-item');
            var tabsWidth = 0;

            tabs.each(function (i) {
                tabsWidth += $(tabs[i]).outerWidth();
            });

            expect(tabsWidth).to.be.below($(responsiveTabs).width());
        });
    });
});
