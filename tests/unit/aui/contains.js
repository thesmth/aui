'use strict';

import contains from '../../../src/js/aui/contains';

describe('aui/contains', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            contains: contains
        });
    });
});
