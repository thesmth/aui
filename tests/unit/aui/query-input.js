'use strict';

import '../../../src/js/aui';
import AJS from '../../../src/js/aui-experimental';
import QueryInput from '../../../src/js/aui/query-input';

describe('aui/query-input', function () {
    var queryInput;

    beforeEach(function () {
        var $input = $('<input/>', { 'id': 'my-thing'}).appendTo('#test-fixture');
        queryInput = new AJS.QueryInput({ el: $input });
    });

    describe('construction', function () {
        it('will attach element to query input', function () {
            expect(queryInput.$el.attr('id')).to.equal('my-thing');
        });
    });

    it('globals', function () {
        expect(AJS).to.contain({
            QueryInput: QueryInput
        });
    });

    describe('changing values', function () {
        it('can retrieve input value', function () {
            expect(queryInput.val()).to.equal('');
        });

        it('can set input value', function () {
            var myval = 'foo';
            queryInput.val(myval);

            expect(queryInput.val()).to.equal(myval);
        });

        it('can listen for changes to the input value', function () {
            var myval = 'foo';
            var callback = sinon.spy();

            queryInput.val(myval);
            queryInput.bind('change', callback);
            queryInput.changed();

            callback.should.have.been.calledOnce;
            expect(callback.calledWith(myval)).to.be.ok;
        });

        it('(BEHAVIOUR WILL CHANGE) will cause queryInput to fire a change event by the time keyup fires', function () {
            var myval = 'foo';
            var callback = sinon.spy();

            queryInput.val(myval);
            queryInput.bind('change', callback);
            queryInput.$el.trigger('keyup');

            callback.should.have.been.calledOnce;
            expect(callback.calledWith(myval)).to.be.ok;
        });

        it('will only trigger a change event if input is actually changed', function () {
            var callback = sinon.spy();

            queryInput.bind('change', callback);
            queryInput.val('foo');
            queryInput.$el.trigger('keyup');
            queryInput.$el.trigger('keyup');

            callback.should.have.been.calledOnce;
        });

        it('will trigger a change event if input changes', function () {
            var callback = sinon.spy();

            queryInput.bind('change', callback);
            queryInput.val('foo');
            queryInput.$el.trigger('keyup');

            callback.should.have.been.calledOnce;

            queryInput.val('bar');
            queryInput.$el.trigger('keyup');

            callback.should.have.been.calledTwice;
        });
    });
});
