'use strict';

import '../../../src/js/aui/button';
import skate from 'skatejs';
import helpers from '../../helpers/all';

var button;

describe('aui/button', function() {
    beforeEach(function() {
        button = helpers.fixtures({
            button: '<button class="aui-button"></button>'
        }).button;
        skate.init(button);
    });

    afterEach(function() {
        $(button).remove();
    });

    it('Button element has prototype', function () {
        expect(button.busy).to.be.a('function');
        expect(button.idle).to.be.a('function');
    });

    it('Calling busy should set aria-busy', function () {
        button.busy();

        expect(button.getAttribute('aria-busy')).to.equal('true');
    });

    it('Calling busy should add spin container inside button', function () {
        button.busy();
        expect(button.querySelectorAll('.spinner').length).to.equal(1);
    });

    it('Calling busy twice should only add one spin container inside button', function () {
        button.busy();
        button.busy();
        expect(button.querySelectorAll('.spinner').length).to.equal(1);
    });

    it('Calling idle should unset aria-busy', function () {
        button.busy();
        button.idle();

        expect(button.hasAttribute('aria-busy')).to.be.false;
    });

    it('Calling idle should remove spin container from inside button', function () {
        button.busy();
        button.idle();
        expect(button.querySelectorAll('.aui-button-spinner').length).to.equal(0);
    });

    it('Calling isBusy returns false initially', function () {
        expect(button.isBusy()).to.be.false;
    });

    it('Calling isBusy returns true when busy', function () {
        button.busy();
        expect(button.isBusy()).to.be.true;
    });

    it('Calling isBusy returns false when idled', function () {
        button.busy();
        button.idle();
        expect(button.isBusy()).to.be.false;
    });
});
