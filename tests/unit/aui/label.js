'use strict';

import helpers from '../../helpers/all';

describe('aui/label', function() {
    var label;
    var LABEL_FOR = 'test-for';
    var LABEL_FORM = 'test-form';
    var LABEL_CONTENT = 'test content';

    function createLabel(options) {
        if (!options) { options = {} };
        var labelFor = options.for ? `for="${options.for}"` : '';
        var labelForm = options.form ? `form="${options.form}"` : '';
        var labelContent = options.content ? options.content : '';
        var dom = helpers.fixtures({
            label: `<aui-label ${labelFor} ${labelForm}>${labelContent}</aui-label>`
        });
        return dom.label;
    }

    describe('Label - construction and initialisation', function() {
        describe('basic initialisation', function() {
            beforeEach(function() {
                label = createLabel({'content': LABEL_CONTENT});
            });

            it('creates a native label child element', function() {
                expect(label._label.tagName).to.equal('LABEL');
            });

            it('moves the content into the child label', function() {
                expect(label._label.textContent).to.equal(LABEL_CONTENT);
            });
        });

        describe('initialising attributes', function() {
            beforeEach(function() {
                label = createLabel({for: LABEL_FOR, form: LABEL_FORM, content: LABEL_CONTENT});
            }) ;

            it('copies the \'for\' attribute to the child label and appends \'input\' to it', function() {
                expect(label._label.getAttribute('for')).to.equal(LABEL_FOR + '-input');
            });

            it('copies the \'form\' attribute to the child label', function() {
                expect(label._label.getAttribute('form')).to.equal(LABEL_FORM);
            });
        });
    });

    describe('Label - behaviour', function() {
        it('handles attributes being added to the label', function(done) {
            label = createLabel();
            expect(label._label.hasAttribute('for')).to.be.false;
            expect(label._label.hasAttribute('form')).to.be.false;

            label.setAttribute('for', LABEL_FOR);
            label.setAttribute('form', LABEL_FORM);

            helpers.afterMutations(function() {
                expect(label._label.getAttribute('for')).to.equal(LABEL_FOR + '-input');
                expect(label._label.getAttribute('form')).to.equal(LABEL_FORM);
                done();
            });
        });

        it('handles attributes being modified', function() {
            label = createLabel({for: LABEL_FOR, form: LABEL_FORM});
            expect(label._label.getAttribute('for')).to.equal(LABEL_FOR + '-input');
            expect(label._label.getAttribute('form')).to.equal(LABEL_FORM);

            var LABEL_FOR_NEW = 'test-for-new';
            label.setAttribute('for', LABEL_FOR_NEW);
            expect(label._label.getAttribute('for')).to.equal(LABEL_FOR_NEW + '-input');

            var LABEL_FORM_NEW = 'test-form-new';
            label.setAttribute('form', LABEL_FORM_NEW);
            expect(label._label.getAttribute('form')).to.equal(LABEL_FORM_NEW);
        });

        it('handles attributes being removed from the label', function() {
            label = createLabel({for: LABEL_FOR, form: LABEL_FORM});
            expect(label._label.hasAttribute('for')).to.be.true;
            expect(label._label.hasAttribute('form')).to.be.true;

            label.removeAttribute('for');
            expect(label._label.hasAttribute('for')).to.be.false;

            label.removeAttribute('form');
            expect(label._label.hasAttribute('form')).to.be.false;
        });
    });

});
