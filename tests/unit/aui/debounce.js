'use strict';

describe('aui/debounce', function () {
    it('globalize debounce', function () {
        expect(AJS.debounce).to.be.exist;
    });

    it('globalize debounceImmediate', function () {
        expect(AJS.debounceImmediate).to.be.exist;
    });
});
