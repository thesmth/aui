'use strict';

import * as events from '../../../src/js/aui/events';

describe('aui/events', function () {
    it('global', function () {
        expect(AJS.events).to.be.defined;
    });
});
