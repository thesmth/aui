'use strict';

import onTextResize from '../../../src/js/aui/on-text-resize';

describe('aui/on-text-resize', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            onTextResize: onTextResize
        });
    });
});
