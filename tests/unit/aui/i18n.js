'use strict';

import AJS from '../../../src/js/aui';
import i18n from '../../../src/js/aui/i18n';

describe('aui/i18n', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            I18n: i18n
        });
    });

    it('return key test', function () {
        expect(AJS.I18n.getText('test.key')).to.equal('test.key');
    });

    describe('returns the value for a defined key', function () {
        beforeEach(function () {
            AJS.I18n.keys['test.key'] = 'This is a Value';
            AJS.I18n.keys['test.formatting.key'] = 'Formatting {0}, and {1}';

        });

        afterEach(function () {
            delete AJS.I18n.keys['test.key'];
            delete AJS.I18n.keys['test.formatting.key'];
        });

        it('with no formatting', function () {
            expect(AJS.I18n.getText('test.key')).to.equal('This is a Value');
        });

        it('with formatting', function () {
            expect(AJS.I18n.getText('test.formatting.key', 'hello', 'world')).to.equal('Formatting hello, and world');
        });
    });
});
