export default `
    <body class="aui-page-sidebar aui-sidebar-expanded">
        <div id="page">
            <!-- #header -->
            <section id="content" role="main">
                <div class="aui-sidebar " >
                    <div class="aui-sidebar-wrapper">
                        <div class="aui-sidebar-body">
                            <header class="aui-page-header">
                                <div class="aui-page-header-inner">

                                </div>
                                <!-- .aui-page-header-inner -->
                            </header>
                            <!-- .aui-page-header -->
                            <nav class="aui-navgroup aui-navgroup-vertical">
                                <div class="aui-navgroup-inner">

                                </div>
                            </nav>
                        </div>
                        <div class="aui-sidebar-footer"><a href="http://example.com/" class="aui-button aui-button-subtle aui-sidebar-settings-button" data-tooltip="Settings"><span class="aui-icon aui-icon-small aui-iconfont-configure"></span><span class="aui-button-label">Settings</span></a><a class="aui-button aui-button-subtle aui-sidebar-toggle aui-sidebar-footer-tipsy" data-tooltip="Expand sidebar ( [ )" href="#"><span class="aui-icon aui-icon-small"></span></a></div>
                    </div>
                </div>
                <header class="aui-page-header">
                    <div class="aui-page-header-inner">
                        <div class="aui-page-header-main">
                            <h1>Settings</h1>
                        </div>
                        <!-- .aui-page-header-main -->
                    </div>
                    <!-- .aui-page-header-inner -->
                </header>
                <!-- .aui-page-header -->
                <div class="aui-page-panel">
                    <div class="aui-page-panel-inner">

                        <!-- .aui-page-panel-content -->
                    </div>
                    <!-- .aui-page-panel-inner -->
                </div>
                <!-- .aui-page-panel -->
            </section>
        </div>
        <!-- #page -->
    </body>
`;
