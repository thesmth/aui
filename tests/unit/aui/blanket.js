'use strict';

import * as blanket from '../../../src/js/aui/blanket';
import AJS from '../../../src/js/aui';

function getBlanketZIndex () {
    return AJS.dim.$dim.css('z-index');
}

describe('aui/blanket', function () {
    var clock;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    it('globals', function () {
        expect(AJS.blanket).to.be.defined;
    });

    it('dim() will update the z-index accordingly', function () {
        AJS.dim(false, 5);
        expect(getBlanketZIndex()).to.equal('5');
        AJS.dim(false, 10);
        expect(getBlanketZIndex()).to.equal('10');
    });

    it('dim() will not created multiple blankets on duplicate calls', function () {
        AJS.dim();
        AJS.dim();
        expect(AJS.$('.aui-blanket').length).to.equal(1);
    });

    it('dim() and undim() will hide all blankets that were previously visible', function () {
        AJS.dim();
        AJS.undim();

        var noBlanket = !AJS.dim.$dim;
        var invisibleBlanket = AJS.dim.$dim.attr('aria-hidden') === 'true';
        expect(noBlanket || invisibleBlanket).to.be.true;
    });

    it('should ensure that the blanket is not displayed after hidden', function () {
        AJS.dim();
        AJS.undim();

        var dim = AJS.dim.$dim;

        clock.tick(1000);
        expect(AJS.dim.$dim.css('visibility')).to.equal('hidden');
    });

    it('Calling dim() twice restores correctly', function () {
        AJS.dim();
        AJS.dim();
        AJS.undim();
        expect(AJS.$('body').css('overflow')).to.eql('visible');
    });

    describe('overflow', function () {
        describe('on <body>', function () {
            it('is reverted to visible on undim if it was originally visible', function () {
                AJS.$('body').css({overflow: ''});
                AJS.dim();
                AJS.undim();
                expect(AJS.$('body').css('overflow')).to.equal('visible');
            });

            it('is set to hidden on undim if it was originally hidden', function () {
                AJS.$('body').css({overflow: 'hidden'});
                AJS.dim();
                AJS.undim();
                expect(AJS.$('body').css('overflow')).to.equal('hidden');
            });
        });

        describe('on <html>', function () {
            it('is reverted to visible on undim if it was originally visible', function () {
                //have to query the dom for the default in this case because ie7 does not adhere to the spec
                var overrideDefault = AJS.$('html').css('overflow');
                AJS.dim();
                AJS.undim();
                expect(AJS.$('html').css('overflow')).to.equal(overrideDefault);
            });

            it('is set to hidden on undim if it was originally hidden', function () {
                AJS.$('html').css({overflow: 'hidden'});
                AJS.dim();
                AJS.undim();
                expect(AJS.$('html').css('overflow')).to.equal('hidden');
            });
        });
    });
});
