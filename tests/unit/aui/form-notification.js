'use strict';

import '../../../src/js/aui/form-notification';
import helpers from '../../helpers/all';
import skate from 'skatejs';

describe('aui/form-notification', function () {
    describe('Form notification Unit Tests -', function () {
        var clock;
        var onHashChangeHandler = null;

        function createInput (attributes) {
            var $input = AJS.$('<input type="text">');
            attributes = $.extend({}, attributes, {'data-aui-notification-field': ''});
            $.each(attributes, function(key, value){
                $input.attr(key, value);
            });
            $('#test-fixture').append($input);
            skate.init($input);
            return $input;
        }

        function countTipsysOnPage () {
            return $('.tipsy').length;
        }

        function firstTipsyOnPage () {
            return $('.tipsy').first();
        }

        function setupLinkEnvironment () {
            clock.tick(100);

            return createInput({
                'data-aui-notification-info': '["Some text here followed by a link <a href="#link-to-google">click here</a>"]'
            });
        }

        beforeEach(function () {
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            $('.tipsy').remove();
            clock.restore();
            if (onHashChangeHandler !== null) {
                $(window).off('hashchange', onHashChangeHandler);
                onHashChangeHandler = null;
                window.location.hash = '';
            }
        });

        it('Fields can receive notifications', function () {
            var $input = createInput({
                'data-aui-notification-info': 'Test info message'
            });

            expect(countTipsysOnPage()).to.equal(0);
            helpers.focus($input[0]);
            expect(countTipsysOnPage()).to.equal(1);
        });

        it('Field notifications receive correct notification class', function () {
            var $input = createInput({
                'data-aui-notification-error': 'Test error message'
            });
            helpers.focus($input[0]);
            expect($('.aui-form-notification-tooltip-error').length).to.equal(1);
            expect($('.aui-form-notification-tooltip-info').length).to.equal(0);
        });

        it('Field notification messages stack correctly', function () {
            var $input = createInput({
                'data-aui-notification-info': '["Test JSON message 1","Test JSON message 2"]'
            });
            helpers.focus($input[0]);
            expect($('.tipsy-inner')[0].innerHTML.indexOf('<li>')).to.not.equal(-1);
        });

        it('There is only one tooltip on the page after focusing multiple', function () {
            var $input1 = createInput({
                'data-aui-notification-info': 'The first message'
            });

            var $input2 = createInput({
                'data-aui-notification-info': 'The second message'
            });
            expect(countTipsysOnPage()).to.equal(0);
            helpers.focus($input1[0]);
            expect(countTipsysOnPage()).to.equal(1);
            helpers.blur($input1[0]);
            helpers.focus($input2[0]);
            expect(countTipsysOnPage()).to.equal(1);

        });

        it('Field notification links are followed', function (done) {
            var $input = setupLinkEnvironment();
            helpers.focus($input[0]);
            expect(countTipsysOnPage()).to.equal(1);

            $(window).one('hashchange', onHashChangeHandler = function () {
                expect(window.location.hash).to.equal('#link-to-google');
                done();
            });
            $('a')[0].click();
        });

        it('Notification states can be changed after a field is created', function(done) {
            var $input = createInput({
                'data-aui-notification-info': 'Test info message'
            });
            skate.init($input[0]);

            helpers.focus($input[0]);
            $input.attr('data-aui-notification-error', 'Test error message');

            helpers.afterMutations(function () {
                expect(countTipsysOnPage()).to.equal(1);
                var numberOfErrorNotifications = $('.aui-form-notification-tooltip-error').length;
                numberOfErrorNotifications.should.equal(1);
                done();
            });
        });

        it('Clicking on the notification when the field is focused keeps the notification open', function () {
            var $input = setupLinkEnvironment();
            helpers.focus($input[0]);
            var $tipsy = firstTipsyOnPage();
            $tipsy.click();
            expect(countTipsysOnPage()).to.equal(1);
        });

        it('Clicking on the document when a field is shown closes the notification', function () {
            var $input = createInput({
                'data-aui-notification-info': 'Test info message that should be hidden after clicking'
            });

            helpers.focus($input[0]);
            expect(countTipsysOnPage()).to.equal(1);

            helpers.mousedown($('#test-fixture').get(0));

            expect(countTipsysOnPage()).to.equal(0);
        });

        it('Tooltips are unaffected by blur when on unfocusable elements', function () {
            var $nonInput = $('<div data-aui-notification-field="" data-aui-notification-info="Basic message"/>');
            $('#test-fixture').append($nonInput);
            skate.init($nonInput);

            expect(countTipsysOnPage()).to.equal(1);
            helpers.focus($nonInput[0]);
            helpers.blur($nonInput[0]);

            expect(countTipsysOnPage()).to.equal(1);
        });

        it('Tooltips are unaffected by mousedown when on unfocusable elements', function () {
            var $nonInput = $('<div data-aui-notification-field="" data-aui-notification-info="Basic message"/>');
            $('#test-fixture').append($nonInput);
            skate.init($nonInput);

            expect(countTipsysOnPage()).to.equal(1);
            $('body').click();

            expect(countTipsysOnPage()).to.equal(1);
        });

        it('Text input field tooltips become visible / invisible after switching between fields', function () {
            var $input1 = createInput({
                'data-aui-notification-info': 'This is the first input'
            });
            var $input2 = createInput({
                'data-aui-notification-info': 'This is the second input'
            });

            expect(countTipsysOnPage()).to.equal(0);
            helpers.focus($input1[0]);
            expect(countTipsysOnPage()).to.equal(1);
            helpers.focus($input2[0]);
            expect(countTipsysOnPage()).to.equal(1);
        });
    });
});
