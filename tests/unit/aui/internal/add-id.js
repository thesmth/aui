'use strict';

import addId from '../../../../src/js/aui/internal/add-id';

describe('aui/internal/add-id', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            _addID: addId
        });
    });
});
