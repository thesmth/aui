'use strict';

import AJS from '../../src/js/aui';
import CustomEvent from '../../src/js/aui/polyfills/custom-event';

function dispatch (event, target, data) {
    var orig = target;
    target = (typeof target === 'string') ? $(target) : target;
    target = (target instanceof $) ? target[0] : target;

    if (typeof event === 'string') {
        event = new CustomEvent(event, {
            bubbles: true,
            cancelable: true,
            detail: data
        });
    }

    if (!target || typeof target.dispatchEvent !== 'function') {
        var msg = AJS.format('The object provided to dispatch events to did not resolve to a DOM element: was {0}', String(orig));
        var err = new Error(msg);
        err.target = target;
        throw err;
    }

    target.dispatchEvent(event);
}

function ensureHtmlElement (el) {
    if (typeof el === 'string') {
        var div = document.createElement('div');
        div.innerHTML = el;
        return div.children.item(0);
    }

    if (el instanceof $) {
        return el.get(0);
    }

    return el;
}

function getLayers () {
    return $('.aui-layer');
}

function createFixtureItems (fixtureItems, removeOldFixtures) {
    var fixtureElement = document.getElementById('test-fixture');

    if (removeOldFixtures || removeOldFixtures === undefined) {
        fixtureElement.innerHTML = '';
    }

    if (fixtureItems) {
        for (var name in fixtureItems) {
            fixtureItems[name] = ensureHtmlElement(fixtureItems[name]);
            fixtureElement.appendChild(fixtureItems[name]);
        }
    }

    return fixtureItems;
}

function removeLayers () {
    var $layer;

    while ($layer = AJS.LayerManager.global.getTopLayer()) {
        AJS.LayerManager.global.popUntil($layer);
        $layer.remove();
    }

    getLayers().remove();
    AJS.undim();
    $('.aui-blanket').remove();
}

function warnIfLayersExist () {
    if (getLayers().length) {
        // Need to bind to console for chrome, otherwise it throws an illegal invocation error.
        console.log('Layers have been left in the DOM. These must be removed from the BODY to ensure they don\'t affect other tests. Use helpers.removeLayers().');
        removeLayers();
    }
}

function click (element) {
    dispatch('click', element);
}

function mousedown (element) {
    dispatch('mousedown', element);
}

function hover (element) {
    ['mouseenter','mouseover','mousemove'].forEach(function (name) {
        dispatch(name, element);
    });
}

function pressKey (keyCode, modifiers, onElement) {
    var e = new CustomEvent('keydown', {
        bubbles: true,
        cancelable: true
    });

    modifiers = modifiers || {};

    if (typeof keyCode === 'string') {
        var ucKeyCode = keyCode.toUpperCase();

        if (typeof AJS.keyCode[ucKeyCode] === 'number') {
            keyCode = AJS.keyCode[ucKeyCode];
        } else {
            keyCode = keyCode.charCodeAt(0);
        }
    }

    e.keyCode = keyCode;
    e.ctrlKey = !!modifiers.control;
    e.shiftKey = !!modifiers.shift;
    e.altKey = !!modifiers.alt;
    e.metaKey = !!modifiers.meta;

    dispatch(e, onElement || document.activeElement);

}

function fakeTypingOut (stringInput, onElement) {
    onElement = onElement || document.activeElement;

    String(stringInput).split('').forEach(function(char) {
        pressKey(char, onElement);
        onElement.value += char;

        if (setTimeout.clock) {
            setTimeout.clock.tick(1);
        }
    });
}

function fakeBackspace (onElement, numTimes) {
    onElement = onElement || document.activeElement;
    numTimes = typeof numTimes === 'number' ? numTimes : 1;

    _(numTimes).times(function () {
        pressKey(onElement);
        onElement.value = onElement.value.substring(0, onElement.value.length - 1);

        if (setTimeout.clock) {
            setTimeout.clock.tick(1);
        }
    });
}

function fakeClear (withKey, onElement) {
    onElement = onElement || document.activeElement;
    fakeBackspace(onElement, onElement.value.length);
    pressKey(withKey || 'escape');
}

var realTimeout = window.setTimeout;
function afterMutations (callback) {
    realTimeout(callback, 1);
}

function respondWithJson (server, pattern, json) {
    if (arguments.length === 2) {
        json = pattern;
        pattern = /.*/;
    }

    server.respondWith(pattern, [200, 'application/json', JSON.stringify(json)]);
}

function focus($element){
    var element = $($element)[0];
    element.focus();
    element.dispatchEvent(new CustomEvent('focus'));
}

function blur($element) {
    var element = $($element)[0];
    element.blur();
    element.dispatchEvent(new CustomEvent('blur'));
}

// Chai extensions
// ---------------
chai.use(function (chai, utils) {
    utils.addProperty(chai.Assertion.prototype, 'visible', function () {
        var $el = $(this._obj);
        this.assert(
            $el.is(':visible') === true,
            'expected "#{this}" to be visible',
            'expected "#{this}" to be hidden'
        );
    });
});

export default {
    afterMutations: afterMutations,
    click: click,
    dispatch: dispatch,
    ensureHtmlElement: ensureHtmlElement,
    mousedown: mousedown,
    fakeBackspace: fakeBackspace,
    fakeClear: fakeClear,
    fakeTypingOut: fakeTypingOut,
    fixtures: createFixtureItems,
    hover: hover,
    pressKey: pressKey,
    removeLayers: removeLayers,
    respondWithJson: respondWithJson,
    warnIfLayersExist: warnIfLayersExist,
    focus: focus,
    blur: blur
};
