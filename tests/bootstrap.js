import helpers from './helpers/all';

var fixtureElement;

mocha.setup('bdd');

/**
 * Sets the el's innerHTML to '' and executes the next callback after any
 * DOM mutation handlers (e.g., skate's detached callbacks) have had a
 * chance to run.
 */
function clearContents(el, next) {
    el.innerHTML = '';
    if (next) {
        helpers.afterMutations(next);
    }
}

beforeEach(function () {
    fixtureElement = document.getElementById('test-fixture');
    if (!fixtureElement) {
        fixtureElement = document.createElement('div');
        fixtureElement.id = 'test-fixture';
        document.body.appendChild(fixtureElement);
    }
    clearContents(fixtureElement);
});

afterEach(function (done) {
    clearContents(fixtureElement, function () {
        helpers.warnIfLayersExist();
        helpers.removeLayers();

        // Clean up after:
        // - blanket
        // - dialog
        // - dialog2
        $('body, html').css('overflow', '');
        $('.aui-blanket, .aui-dialog, .aui-popup').remove();
        done();
    });
});
