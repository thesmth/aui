'use strict';

import './code';
import Sandy from 'sandy';
import skate from 'skatejs';

skate('aui-docs-example', {
    attached: function(el) {
        el.className = 'aui-docs-example';

        var exampleActions = createExampleActions(el);
        el.appendChild(exampleActions);
    }
});

function createExampleActions(el) {
    var id = AJS.id('aui-docs-example-actions-dropdown');
    var exampleActionsContent = `
        <div class="aui-buttons">
            <button class="aui-button aui-button-split-main aui-button-light" data-docs-example-destination="jsbin"><span class="aui-icon aui-icon-small aui-iconfont-edit">Edit icon</span> Edit in jsbin</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more aui-button-light" aria-haspopup="true" aria-controls="${id}" aria-expanded="false">Split more</button>
        </div>
        <div id="${id}" class="aui-dropdown2 aui-style-default aui-layer" aria-hidden="true">
            <ul class="aui-list-truncate">
                <li><a data-docs-example-destination="codepen" href="#">Edit in codepen</a></li>
                <li><a data-docs-example-destination="jsfiddle" href="#">Edit in jsfiddle</a></li>
            </ul>
        </div>
    `;

    var exampleActions = document.createElement('div');
    exampleActions.className = 'aui-docs-example-actions';
    exampleActions.innerHTML = exampleActionsContent;


    function exampleButtonClickHandler (e) {
        var languages = getLanguageContents(el);
        var sandyExample = createSandyExample(languages.html, languages.js, languages.css);
        sandyExample.pushTo(e.target.getAttribute('data-docs-example-destination'));
    }

    var buttons = exampleActions.querySelectorAll('[data-docs-example-destination]');

    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', exampleButtonClickHandler);
    }

    return exampleActions;
}

function getCodeElement (el, lang) {
    return el.querySelector('[type="text/' + lang + '"]');
}

function getLanguageContents (el) {
    var languages = {};
    var css = getCodeElement(el, 'css');
    var html = getCodeElement(el, 'html');
    var js = getCodeElement(el, 'js');

    languages.css = css && css.innerHTML.trim();
    languages.html = html && html.innerHTML.trim();
    languages.js = js && js.innerHTML.trim();

    return languages;
}

function createSandyExample(html, js, css) {
    var config = {
        dependencies: getDependencies()
    };

    if (html) {
        config.html = {content: html};
    }

    if (css) {
        config.css = {content: css};
    }

    if (js) {
        config.js = {content: js};
    }

    return new Sandy(config);
}

function getDependencies() {
    var distLocation = getDistLocation();

    var dependencies = [
        'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js',
        'http://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.15/require.min.js',
        distLocation + 'js/aui.js',
        distLocation + 'js/aui-experimental.js',
        distLocation + 'js/aui-datepicker.js',
        distLocation + 'css/aui.css',
        distLocation + 'css/aui-experimental.css'
    ];

    return dependencies;
}

function getDistLocation() {
    //Inserted by the template at run time based on the presence / absence of the --debug flag
    return document.getElementById('dist-location').innerHTML;
}
