import skate from 'skatejs';
import template from 'skatejs-template-html';

skate('aui-docs-component', {
    template: template('<code>&lt;<content></content>&gt;</code>')
});
