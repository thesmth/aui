---
component: Toggle Button
layout: main-layout.html
---

<a href="https://design.atlassian.com/2.2/product/components/buttons#toggle-button" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<h3>Summary</h3>

<p>A toggle button provides a quick way to see whether a given option is enabled or disabled and to toggle between these states.
    Place a toggle button to the right of a label, in a table, or below a section describing what is enabled or disabled.</p>

<p>A toggle button should be used if the main intent is to make a binary choice,
    such as turning something on or off, and the choice is not part of a form.</p>

<h3>Status</h3>
<table class="aui summary">
    <tbody>
    <tr>
        <th>API status:</th>
        <td><span class="aui-lozenge aui-lozenge-success">experimental</span></td>
    </tr>
    <tr>
        <th>Included in AUI core?</th>
        <td><span class="aui-lozenge aui-lozenge-current">Not in core</span> You must explicitly require the web
            resource key.
        </td>
    </tr>
    <tr>
        <th>Web resource key:</th>
        <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-toggle"><code>com.atlassian.auiplugin:aui-toggle</code>
        </td>
    </tr>
    <tr>
        <th>AMD Module key:</th>
        <td class="resource-key">N/A</td>
    </tr>
    <tr>
        <th>Experimental since:</th>
        <td>5.9</td>
    </tr>
    </tbody>
</table>

<h3>Examples</h3>

<div class="aui-flatpack-example">
    <form class="aui">
        <div class="field-group">
            <aui-label for="my-toggle">Toggle button with clickable label</aui-label>
            <aui-toggle label="toggle button" id="my-toggle" checked></aui-toggle>
        </div>
        <div class="field-group">
            <aui-label for="my-disabled-toggle">Disabled toggle buttons</aui-label>
            <aui-toggle label="checked disabled toggle" id="my-disabled-toggle" checked disabled></aui-toggle> <aui-toggle label="disabled toggle" disabled></aui-toggle>
        </div>
    </form>
</div>

<h3>Code</h3>

<p>Default toggle button</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/html">
        <aui-toggle label="toggle button"></aui-toggle>
    </script><!--</aui-docs-code>-->
</aui-docs-example>

<p>Disabled toggle button with AUI label</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/html">
        <aui-label for="my-toggle">Disabled toggle button</aui-label>
        <aui-toggle label="disabled toggle" id="my-toggle" disabled></aui-toggle>
    </script><!--</aui-docs-code>-->
</aui-docs-example>

<h3 id="attributes">Attributes and properties</h3>

<p>Attributes and properties can be set and accessed directly on the <code>aui-toggle</code> element.</p>

<aui-docs-example>
    <script is="aui-docs-code" type="text/js">
        var toggle = document.getElementById('my-toggle');
        toggle.checked = true;
        toggle.disabled = true;
    </script><!--/aui-docs-code-->
</aui-docs-example>

<table class="aui">
    <thead>
    <tr>
        <th>Name</th>
        <th>Attribute</th>
        <th>Property</th>
        <th>Type</th>
        <th class="description">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>label</code> <span class="aui-lozenge aui-lozenge-current">required</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>Sets accessibility text for toggle button.</p>
        </td>
    </tr>
    <tr>
        <td><code>checked</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td>If set, the toggle will be set to the 'on' state.</td>
    </tr>
    <tr>
        <td><code>disabled</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td>If set, the user will not be able to interact with the toggle.</td>
    </tr>
    <tr>
        <td><code>form</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-warning">is a read-only property</span></td>
        <td>String</td>
        <td>
            <p>The <code>form</code> element that the toggle button is associated with. The attribute is the id of the <code>form</code> element, while the read-only property returns the element.</p>
            <p><i>HTML5 attribute. Not supported in IE10 or older.</i></p>
        </td>
    </tr>
    <tr>
        <td><code>name</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>The name of the toggle button element, used to reference the element in JavaScript or form data.</td>
    </tr>
    <tr>
        <td><code>value</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>The value associated with the toggle button element (sent on form submission).</td>
    </tr>
    <tr>
        <td><code>busy</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-close-dialog">is not an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td>
            <p>Sets the toggle to a busy state, displaying the busy spinner.</p>
        </td>
    </tr>
    <tr>
        <td><code>tooltip-on</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>Sets the tooltip text that is shown when the toggle is set to the ‘on’ state.</p>
            <script is="aui-docs-code" type="text/js">
                toggle.setAttribute('tooltip-on', 'Enabled');
                toggle.tooltipOn = 'Enabled';
            </script><!--/aui-docs-code-->
        </td>
    </tr>
    <tr>
        <td><code>tooltip-off</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>String</td>
        <td>
            <p>Sets the tooltip text that is shown when the toggle is set to the ‘off’ state.</p>
            <script is="aui-docs-code" type="text/js">
                toggle.setAttribute('tooltip-off', 'Disabled');
                toggle.tooltipOff = 'Disabled';
            </script><!--/aui-docs-code-->
        </td>
    </tr>
    </tbody>
</table>

<h3>Asynchronous toggle button</h3>

<p>The busy property can be used to handle asynchronous requests. Bind to the <code>change</code> event
    to trigger an AJAX request and display a spinner when the toggle is pressed:</p>

<aui-docs-example>
    <script is="aui-docs-code" type="text/js">
        var toggle = document.getElementById('my-toggle');
        toggle.addEventListener('change', function(e) {
            var isChecked = toggle.checked;     // new value of the toggle
            toggle.busy = true;
            $.post('set/my/variable', { value: isChecked })
                .done(function () {
                    console.log('success');
                })
                .fail(function () {
                    toggle.checked = !isChecked;
                    console.error('display an error message');
                })
                .always(function () {
                    toggle.busy = false;
                });
        });
    </script><!--/aui-docs-code-->
</aui-docs-example>
