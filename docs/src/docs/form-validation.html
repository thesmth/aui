---
component: Form validation
layout: main-layout.html
---

<a href="https://design.atlassian.com/latest/product/components/forms/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<h3>Summary</h3>

<p>Form validation is used to provide an
    interface for validating form fields, and displaying feedback on problems.</p>

<h3>Status</h3>
<table class="aui summary">
    <tbody>
    <tr>
        <th>API status:</th>
        <td><span class="aui-lozenge aui-lozenge-current">experimental</span></td>
    </tr>
    <tr>
        <th>Included in AUI core?</th>
        <td><span class="aui-lozenge aui-lozenge-current">Not in core</span> You must explicitly require the web
            resource key.
        </td>
    </tr>
    <tr>
        <th>Web resource key:</th>
        <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-form-validation"><code>com.atlassian.auiplugin:aui-form-validation</code>
        </td>
    </tr>
    <tr>
        <th>AMD Module key:</th>
        <td class="resource-key" data-amd-key="aui/form-validation"><code>aui/form-validation</code></td>
    </tr>
    <tr>
        <th>Experimental since:</th>
        <td>5.5</td>
    </tr>
    </tbody>
</table>

<h3>Examples</h3>
<div class="aui-flatpack-example">
    <form class="aui">
        <div class="field-group">
            <label for="demo-message-length">Input with validation</label>
            <input id="demo-message-length"
                   class="text" type="text"
                   data-aui-validation-field minlength="10">
        </div>
    </form>
</div>
<script>
    AJS.$(document).ready(function () {
// begin demo javascript

        require(['aui/form-validation']);

// end demo javascript
    });
</script>
<h3>Code</h3>
<h4>Setup</h4>
<p>Ensure you require the Form Validation module in your javascript initialisation code, as
    well as the AUI-provided field validators.</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/js">
        require(['aui/form-validation']);
    </script><!--</aui-docs-code>-->
    <script is="aui-docs-code" type="text/html">
        <form class="aui">
            <div class="field-group">
                <label for="demo-message-length">Input
                    with validation</label>
                <input id="demo-message-length" class="text" type="text" data-aui-validation-field minlength="10">
            </div>
        </form>
    </script><!--</aui-docs-code>-->
</aui-docs-example>

<p>Note the use of the <code>minlength</code>
    attribute. This attribute specifies minimum length of the input value (the length must be at least 10 characters, or
    validation will fail).</p><p>In addition, <code>data-aui-validation-field</code> must be present for validation to occur.</p>
<p>This is enough for validation to be set up on the field – no further initialisation is necessary. The library will
    take care of binding events and adding markup to any input with a <code>data-aui-validation-*</code> data attribute.
</p><h3>Options</h3><h4>Markup configuration</h4><p>Form validation arguments and options are expressed through
    markup.</p><h5>Provided validators</h5><p>Arguments for the provided validators are configured in data attributes
    beginning with the <code>data-aui-validation-*</code>prefix.</p>
<table class="aui api-table">
    <thead>
    <tr>
        <th scope="col">Data attribute</th>
        <th scope="col">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <ul>
                <li><code>data-aui-validation-minlength</code> <span class="aui-lozenge aui-lozenge-error">Deprecated</span></li>
                <li><code>data-aui-validation-maxlength</code> <span class="aui-lozenge aui-lozenge-error">Deprecated</span></li>
                <li><code>minlength</code></li>
                <li><code>maxlength</code></li>
            </ul>
        </td>
        <td>The length of the field's value must be less than or equal to the <code>minlength</code>, and greater than
            or equal to the <code>maxlength</code>.
        </td>
    </tr>
    <tr>
        <td><code>data-aui-validation-matchingfield</code></td>
        <td>The <code>id</code> of an input that must have a matching value</td>
    </tr>
    <tr>
        <td><code>data-aui-validation-doesnotcontain</code></td>
        <td>Some text that the value of the field cannot contain</td>
    </tr>
    <tr>
        <td>
            <ul>
                <li><code>data-aui-validation-pattern</code> <span class="aui-lozenge aui-lozenge-error">Deprecated</span></li>
                <li><code>pattern</code></li>
            </ul>
        </td>
        <td>A regex pattern that the field must match</td>
    </tr>
    <tr>
        <td>
            <ul>
                <li><code>data-aui-validation-required</code> <span class="aui-lozenge aui-lozenge-error">Deprecated</span></li>
                <li><code>required</code></li>
            </ul>
        </td>
        <td>This is a required field, and the field's value must have a length greater than zero.</td>
    </tr>
    <tr>
        <td>
            <ul>
                <li><code>data-aui-validation-min</code> <span class="aui-lozenge aui-lozenge-error">Deprecated</span></li>
                <li><code>data-aui-validation-max</code> <span class="aui-lozenge aui-lozenge-error">Deprecated</span></li>
                <li><code>min</code></li>
                <li><code>max</code></li>
            </ul>
        </td>
        <td>The numerical value of this field must be greater than or equal to this minimum. Note that it is different
            to <code>minlength / maxlength</code>, as it compares a number's value, rather than a string's length.
        </td>
    </tr>
    <tr>
        <td><code>data-aui-validation-dateformat</code></td>
        <td>A date format that this field must comply with. Can contain any separator symbols, or the following symbols,
            which represent different date components:
            <ul>
                <li>Y: Four digit year, eg. 2014</li>
                <li>y: Two digit year, eg. 14</li>
                <li>m: Numerical month, eg. 03</li>
                <li>M: Abbreviated month, eg. Mar</li>
                <li>D: Abbreviated day, eg. Mon</li>
                <li>d: Numerical day, eg. 28</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>
            <ul>
                <li><code>data-aui-validation-minchecked</code></li>
                <li><code>data-aui-validation-maxchecked</code></li>
            </ul>
        </td>
        <td>The number of checkboxes checked in this field must be greater than or equal to<code>data-aui-validation-minchecked</code>,
            and less than or equal to <code>data-aui-validation-maxchecked</code></td>
    </tr>
    </tbody>
</table>
<h5>Provided validators messages</h5>
<p>All of the above validators take an additional argument: <code>data-aui-validation-...-msg</code>.
    This sets a custom message that will be shown to the user when the validation fails.</p>
<p>Each argument is passed to <code>AJS.format</code>, with the value of the argument passed in as the first
    value. <code>{0}</code> will be replaced with the arguments value. For example,</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/html">
        <form class="aui">
            <input class="text" type="text" data-aui-validation-field data-aui-validation-pattern="http://www\..+\.com" data-aui-validation-pattern-msg="{0} is not a valid URL">
        </form>
    </script><!--</aui-docs-code>-->
    <script is="aui-docs-code" type="text/js">
        require(['aui/form-validation']);
    </script><!--</aui-docs-code>-->
</aui-docs-example>
<p>The exception to this is <code>data-aui-validation-matchingfield</code>,
    which is passed the first field's argument in <code>{0}</code>and the second field's argument in <code>{1}</code>.
</p><h5>Validation options</h5><p>Options affect the behaviour of all validators running on a field. They are configured
    in data attributes.</p>
<table class="aui api-table">
    <thead>
    <tr>
        <th scope="col">Data attribute</th>
        <th scope="col">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>data-aui-validation-when</code></td>
        <td>The event that will trigger validation. It can be any DOM event fired on the field, such as
            <code>keyup</code> or <code>change</code>, or a custom event that you will initiate yourself.
            <p>Default: <code>change</code></p>
        </td>
    </tr>
    <tr>
        <td><code>data-aui-validation-watchfield</code></td>
        <td>The <code>id</code> of an additional element that can also trigger validation events (the event specified by
            <code>data-aui-validation-when</code>)
            <p>Default: Unspecified (only watches self)</p>
        </td>
    </tr>
    <tr>
        <td><code>data-aui-validation-displayfield</code></td>
        <td>
            <p>
                The <code>id</code> of the element to decorate when fields become valid or invalid. Icons and tooltips
                will be displayed on this field, but validation events and logic will remain on the original field.
            </p>
            <p>Unspecified (self)</p>
        </td>
    </tr>
    <tr>
        <td><code>data-aui-validation-novalidate</code></td>
        <td>If this argument is present, validation is completely disabled on the field</td>
    <tr>
        <td><code>data-aui-tooltip-position</code></td>
        <td>
            The position that the the tooltip will be displayed on. Can be one of 'top', 'bottom' or 'side'.
            <p><code>"side"</code></p>
        </td>
    </tr>
    </tbody>
</table>
<h4 id="state">Checking validation state</h4>
<p>The validation state of a form can be checked by calling the <code>validate</code> method on the form. This method will also trigger validation on the form.</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/html">
        <form is="aui-form" class="aui">
            <input type="text" class="text" data-aui-validation-field data-aui-validation-minlength="5"/>
        </form>
        <button class="aui-button">Check validation state</button>
    </script>
    <script is="aui-docs-code" type="text/js">
        require(['aui/form-validation']);

        var form = document.querySelector('form');
        var button = document.querySelector('button');

        button.addEventListener('click', function () {
            form.validate(function (state) {
                console.log(state);
            });
        });
    </script><!--</aui-docs-code>-->
</aui-docs-example>
<h4 id="submission">Form submission</h4>
<p>To prevent users from submitting invalid forms, the form validation
    library will intercept <code>submit</code>events that contain invalid, validating, or unvalidated elements. If the
    form is still being validated when a <code>submit</code> event occurs, submission will be delayed until validation
    completes.</p>
<p>When the form is submitted in a valid state, the event <code>aui-valid-submit</code> is triggered.
    If the submit event should be prevented, preventing the <code>aui-valid-submit</code> event will
    prevent <code>submit</code> too.</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/html">
        <form id="valid-submit-form" class="aui">
            <div class="field-group">
                <input class="text" type="text" id="input-one" data-aui-validation-field data-aui-validation-required="required">
            </div>
            <div class="field-group">
                <input class="text" type="text" id="input-two" data-aui-validation-field data-aui-validation-max="10">
            </div>
            <div class="field-group">
                <button class="aui-button" type="submit">Submit</button>
            </div>
        </form>
    </script><!--</aui-docs-code>-->
    <script is="aui-docs-code" type="text/js">
        require(['aui/form-validation'], function () {
            AJS.$('#valid-submit-form').on('aui-valid-submit', function(event) {
                console.log('Data saved');
                event.preventDefault();
            });
        });
    </script><!--</aui-docs-code>-->
</aui-docs-example>


<h4>Field events</h4>
<p>A number of additional events are triggered on fields using the form validation
    library.</p>
<table class="aui api-table">
    <thead>
    <tr>
        <th scope="col">Event name</th>
        <th scope="col">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>aui-stop-typing</code></td>
        <td><p>Triggered on a field when there have been no <code>keyup</code> events for some period of time. Can be
            used as the <code>data-aui-validation-when</code> option for validators.</p></td>
    </tr>
    </tbody>
</table>
<h4>Plugin validators</h4>
<p>Additional validators can be registered and your own validators defined. They can
    be synchronous or asynchronous, and may validate or invalidate based on an element in any way.</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/js">
        //Register a plugin validator that ensures an input field must start with a certain sequence of characters
        require(['aui/form-validation/validator-register'], function(validator) {
            validator.register(['startswith'], function(field) {
                if (field.el.value.indexOf(field.args('startswith')) !== 0){
                    field.invalidate(AJS.format('Input must start with {0}', field.args('startswith')));
                } else {
                    field.validate();
                }
            });
        });

        // Then, in your application or page initialization...
        require(['aui/form-validation', 'my-startswith-validator']);
    </script><!--</aui-docs-code>-->
    <script is="aui-docs-code" type="text/html">
        <form class="aui">
            <div class="field-group">
                <input data-aui-validation-field data-aui-validation-startswith="foo" class="text" type="text"/>
            </div>
        </form>
    </script><!--</aui-docs-code>-->
</aui-docs-example>


<h5>Registering a validator</h5><p>The <code>validator.register</code> function takes the following
    arguments</p>
<table class="aui api-table">
    <thead>
    <tr>
        <th scope="col">Argument name</th>
        <th scope="col">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>trigger</code></td>
        <td><p>The trigger that will cause a validator to be run on an element. Can be either an array of arguments to
            match, or a selector string to match.</p>

            <p>If an array of validation arguments are provided, validation will be triggered on elements with the
                corresponding data attributes specified. For example, <code>['startswith', 'endswith']</code> will match <code>data-aui-validation-startswith</code>
                or <code>data-aui-validation-endswith</code>.</p>

            <p>If a string is provided, validation will be triggered on elements that match this selector. For example,
                a trigger of <code>'[aria-required]'</code> will cause the validator to also be run when the <code>aria-required</code>
                attribute is present.</p></td>
    </tr>
    <tr>
        <td><code>validationFunction</code></td>
        <td>A function containing the logic of the validator. This function takes the argument <code>field</code>(see
            below for more information on this).
        </td>
    </tr>
    </tbody>
</table><h5>Writing the validator</h5><p>The function <code>validationFunction(field)</code> is the core of a validator,
    containing the logic of whether or not a field is valid. It takes the following arguments</p>
<table class="aui api-table">
    <thead>
    <tr>
        <th scope="col">Argument name</th>
        <th scope="col">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>field</code></td>
        <td>The field that the validator is being asked to validate. It contains:
            <ul>
                <li><code>field.el</code>: the field that validation has been requested on.</li>
                <li><code>field.args</code>: An accessor function to retrieve validation arguments from the
                    element.<code>arguments('startswith')</code> will access the value for <code>data-aui-validation-startswith</code>.  If there is no prefixed attribute it will return the value of the unprefixed attribute (<code>startswith</code> in this case).
                </li>
                <li><code>field.validate()</code>: declare that this field has passed validation</li>
                <li><code>field.invalidate(message)</code>: declare that this field has failed validation, and possibly
                    show the user the message provided.
                </li>
            </ul>
        </td>
    </tr>
    </tbody>
</table><p>After performing whatever logic is necessary with the <code>field.el</code> object to validate or
    invalidate, all code paths must execute either <code>field.validate()</code> or
    <code>field.invalidate('message')</code>. The reason given may be shown on the field as an error (see <code>AJS.format()</code>
    for formatting these strings).</p>
