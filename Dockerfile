FROM node:0.12

RUN apt-get update
RUN apt-get install -y openjdk-7-jdk

# from https://registry.hub.docker.com/u/markadams/chromium-xvfb/dockerfile/
RUN apt-get install -y xvfb chromium

ADD build/docker/xvfb-chromium /usr/bin/xvfb-chromium
RUN ln -s /usr/bin/xvfb-chromium /usr/bin/google-chrome
RUN ln -s /usr/bin/xvfb-chromium /usr/bin/chromium-browser
# endfrom

WORKDIR /usr/src/aui
# add package.json/npm install on their own so the install gets cached
ADD package.json /usr/src/aui/package.json
RUN npm install

ADD . /usr/src/aui
RUN npm run prepublish

EXPOSE 7000
