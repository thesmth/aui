# AUI - Atlassian UI

The Atlassian User Interface library.

## Requirements

- Java 1.7 - for building the soy templates.
- Node 0.12+
- NPM

## Installation

NPM install takes care of everything for you.

    npm install

## Building

To build the distribution:

    npm run dist

To build the UMD files:

    npm run umd

## Developing

Although we encourage you write your tests first, then your code, you can test your changes in a couple areas.

### Unit tests

We use [Karma](http://karma-runner.github.io/0.10/index.html) for running our unit tests.

To run tests once:

    npm test

To run test matching a pattern so you don't have to run all of them:

    npm test -- --grep [pattern]

To run tests in watch mode:

    npm test -- --watch

If you're developing a specific feature or fixing a bug it's helpful to combine these:

    npm test -- -wg aui/select

### Visual tests

We have a reference application called the `flatapp`. We use this to visualise our changes during development and testing.

To build the flatapp:

    npm run flatapp

To watch for changes:

    npm run flatapp -- -w

To run the flatapp in your browser:

    npm run flatapp/server

By default this will open up a page at [http://0.0.0.0:7000/pages/](http://0.0.0.0:7000/pages/). It will automatically live-reload. To customise this you can specify a `host` and `port`:

    npm run flatapp/server -- --host localhost --port 80

## Documenting

To build the docs:

    npm run docs/build

To build the docs and watch for changes:

    npm run docs -- --watch

To run the docs in your browser:

    npm run docs

By default this will open up a page at [http://0.0.0.0:8000/](http://0.0.0.0:8000/). It will automatically live-reload. To customise this you can specify a `host` and `port`:

    npm run docs/server -- --host localhost --port 80

## CLI

To see a list of the commands:

    npm run

To see the help for a specific command pass `--help` to it.

## How do you get it?

AUI distributions are released to the [aui-dist repo on Bitbucket](https://bitbucket.org/atlassian/aui-dist).

##Additional documentation

* [Component documentation](https://docs.atlassian.com/aui/latest/)
* [Changelog](https://bitbucket.org/atlassian/aui/src/master/changelog.md?at=master)

## Raising issues

Raise bugs or feature requests in the [AUI project](https://ecosystem.atlassian.net/browse/AUI).

## Contributing

Contributions to AUI are via pull request.

- Create an issue in the [AUI project](https://ecosystem.atlassian.net/browse/AUI). Creating an issue is a good place to
talk the AUI team about whether anyone else is working on the same issue, what the best fix is, and if this is a new feature,
whether it belongs in AUI. If you don't create an issue, we'll ask you to create one when you issue the PR and retag your
commits with the issue key.
- If you have write access to the AUI repo (ie if you work at Atlassian), you can create branches in the main AUI repo -
name your branch as `{issue-key}-{description}`, eg `AUI-1337-fix-the-contributor-guide`. If you don't have
write access, please fork AUI and issue a PR.
- Ensure all commits are tagged with the issue key (`AUI-1337 fixes to contributor guide`).
- Write tests. Unit tests are preferred over integration tests.
- Most PRs will go into master, however you might want them to go into a stable branch. If so, set the target branch
as the stable branch and the AUI team will manage merging stable into master after the PR is through.

## Compatibility

AUI supports the following browsers:

- Chrome latest stable
- Firefox latest stable
- Safari latest stable (on OS X only)
- IE 9+

## License

AUI is released under the [Apache 2 license](https://bitbucket.org/atlassian/aui/src/master/licenses/LICENSE-aui.txt).
See the [licenses directory](https://bitbucket.org/atlassian/aui/src/master/licenses/) for information about AUI and included libraries.
