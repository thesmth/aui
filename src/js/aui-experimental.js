'use strict';

import './aui/polyfills/placeholder';

import './aui/banner';
import './aui/button';
import './aui/checkbox-multiselect';
import './aui/dialog2';
import './aui/expander';
import './aui/flag';
import './aui/form-validation';
import './aui/label';
import './aui/progress-indicator';
import './aui/progressive-data-set';
import './aui/query-input';
import './aui/restful-table';
import './aui/result-set';
import './aui/results-list';
import './aui/select';
import './aui/select2';
import './aui/sidebar';
import './aui/spin';
import './aui/tables-sortable';
import './aui/tipsy';
import './aui/toggle';
import './aui/tooltip';
import './aui/trigger';
import './aui/truncating-progressive-data-set';

export default window.AJS;
