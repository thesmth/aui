'use strict';

import $ from '../jquery';
import amdify from '../internal/amdify';
import skate from 'skatejs';

(function () {
    if ('placeholder' in document.createElement('input')) {
        return;
    }

    skate('placeholder', {
        type: skate.type.ATTRIBUTE,
        attached: function(el) {
            giveInputPlaceholderPolyfill(el);
        }
    });

    function giveInputPlaceholderPolyfill (input) {
        var $input = $(input);
        applyDefaultText($input);

        $input.blur(function () {
            applyDefaultText($input);
        });

        $input.focus(function () {
            if ($input.hasClass('aui-placeholder-shown')) {
                $input.val('');
                $input.removeClass('aui-placeholder-shown');
            }
        });
    }

    function applyDefaultText($input) {
        if (!$.trim($input.val()).length) {
            $input.val($input.attr('placeholder'));
            $input.addClass('aui-placeholder-shown');
        }
    }
})();
