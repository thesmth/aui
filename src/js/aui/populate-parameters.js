'use strict';

import $ from './jquery';
import globalize from './internal/globalize';

function populateParameters (parameters) {
    if (!parameters) {
        parameters = AJS.params = {};
    }

    $('.parameters input').each(function () {
        var value = this.value;
        var id = this.title || this.id;

        if ($(this).hasClass('list')) {
            if (parameters[id]) {
                parameters[id].push(value);
            } else {
                parameters[id] = [value];
            }
        } else {
            parameters[id] = (value.match(/^(tru|fals)e$/i) ? value.toLowerCase() === 'true' : value);
        }
    });
}

globalize('params', {});
globalize('populateParameters', populateParameters);

export default populateParameters;
