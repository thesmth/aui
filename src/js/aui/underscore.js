import _ from '../../js-vendor/underscorejs/underscore';

if(!window._) {
    window._ = _;
}

export default window._;
