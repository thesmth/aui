'use strict';

import $ from './jquery';
import { getMessageLogger } from './internal/deprecation';
import { recomputeStyle } from './internal/animation';
import createElement from './create-element';
import globalize from './internal/globalize';

var $overflowEl;

/**
 * Dims the screen using a blanket div
 * @param useShim deprecated, it is calculated by dim() now
 */
function dim (useShim, zIndex) {
    if  (!$overflowEl) {
        $overflowEl = $(document.body);
    }

    if (useShim === true) {
        useShimDeprecationLogger();
    }

    var isBlanketShowing = (!!dim.$dim) && dim.$dim.attr('aria-hidden') === 'false';

    if (!!dim.$dim) {
        dim.$dim.remove();
        dim.$dim = null;
    }

    dim.$dim = createElement("div").addClass("aui-blanket");
    dim.$dim.attr('tabindex', '0'); //required, or the last element's focusout event will go to the browser
    dim.$dim.appendTo(document.body);

    if (!isBlanketShowing) {
        //recompute after insertion and before setting aria-hidden=false to ensure we calculate a difference in
        //computed styles
        recomputeStyle(dim.$dim);
        dim.cachedOverflow = $overflowEl.css("overflow");
        $overflowEl.css("overflow", "hidden");
    }

    dim.$dim.attr('aria-hidden', 'false');

    if (zIndex) {
        dim.$dim.css({zIndex: zIndex});
    }

    return dim.$dim;
};

/**
 * Removes semitransparent DIV
 * @see dim
 */
function undim () {
    if (dim.$dim) {
        dim.$dim.attr('aria-hidden', 'true');

        $overflowEl && $overflowEl.css("overflow",  dim.cachedOverflow);
    }
};

var useShimDeprecationLogger = getMessageLogger('useShim', {
    extraInfo: 'useShim has no alternative as it is now calculated by dim().'
});

globalize('dim', dim);
globalize('undim', undim);

export {
    dim,
    undim
};
