'use strict';

import $ from './jquery';
import Alignment from './internal/alignment';
import amdify from './internal/amdify';
import enforce from './internal/enforcer';
import globalize from './internal/globalize';
import layer from './layer';
import skate from 'skatejs';

var DEFAULT_HOVEROUT_DELAY = 1000;

function getTrigger (element) {
    return document.querySelector('[aria-controls="' + element.id + '"]');
}

function doIfTrigger (element, callback) {
    var trigger = getTrigger(element);

    if (trigger) {
        callback(trigger);
    }
}

function doIfHover (element, callback) {
    var isHover = element.getAttribute('responds-to') === 'hover';

    if (isHover) {
        callback(element);
    }
}

function initAlignment (element, trigger) {
    if (!element._auiAlignment) {
        element._auiAlignment = new Alignment(element, trigger);
    }
}

function enableAlignment (element, trigger) {
    initAlignment(element, trigger);
    element._auiAlignment.enable();
}

function disableAlignment (element, trigger) {
    initAlignment(element, trigger);
    element._auiAlignment.disable();
}

function handleMessage (element, message) {
    var messageTypeMap = {
        toggle: ['click'],
        hover: ['mouseenter', 'mouseleave', 'focus', 'blur']
    };
    var respondsTo = element.getAttribute('responds-to') || element.getAttribute('data-aui-responds-to');
    var messageList = messageTypeMap[respondsTo];

    if (messageList && messageList.indexOf(message.type) > -1) {
        messageHandler[message.type](element, message);
    }
}

var messageHandler = {
    click: function (element) {
        if (element.isVisible()) {
            if (!layer(element).isPersistent()) {
                element.hide();
            }
        }
        else {
            element.show();
        }
    },

    mouseenter: function (element) {
        if (!element.isVisible()) {
            element.show();
        }

        if (element._clearMouseleaveTimeout) {
            element._clearMouseleaveTimeout();
        }
    },

    mouseleave: function (element) {
        if (layer(element).isPersistent() || !element.isVisible()) {
            return;
        }

        if (element._clearMouseleaveTimeout) {
            element._clearMouseleaveTimeout();
        }

        var timeout = setTimeout(function () {
            if (!element.mouseInside) {
                element.hide();
            }
        }, DEFAULT_HOVEROUT_DELAY);

        element._clearMouseleaveTimeout = function () {
            clearTimeout(timeout);
            element._clearMouseleaveTimeout = null;
        };
    },

    focus: function (element) {
        if (!element.isVisible()) {
            element.show();
        }
    },

    blur: function (element) {
        if (!layer(element).isPersistent() && element.isVisible()) {
            element.hide();
        }
    }
};

var InlineDialog2 = skate('aui-inline-dialog', {
    prototype: {
        /**
         * Shows the inline dialog.
         *
         * @returns {HTMLElement}
         */
        show: function () {
            var that = this;

            layer(this).show();

            doIfTrigger(this, function (trigger) {
                enableAlignment(that, trigger);
                trigger.setAttribute('aria-expanded', 'true');
            });

            return this;
        },

        /**
         * Hides the inline dialog.
         *
         * @returns {HTMLElement}
         */
        hide: function () {
            var that = this;

            layer(this).hide();

            doIfTrigger(this, function (trigger) {
                disableAlignment(that, trigger);
                trigger.setAttribute('aria-expanded', 'false');
            });

            return this;
        },

        /**
         * Returns whether or not the inline dialog is visible.
         *
         * @returns {HTMLElement}
         */
        isVisible: function () {
            return layer(this).isVisible();
        },

        /**
         * Handles the receiving of a message from another component.
         *
         * @param {Object} msg The message to act on.
         *
         * @returns {HTMLElement}
         */
        message: function (msg) {
            handleMessage(this, msg);
            return this;
        }
    },

    created: function (element) {
        doIfTrigger(element, function (trigger) {
            trigger.setAttribute('aria-expanded', element.isVisible());
            trigger.setAttribute('aria-haspopup', 'true');
        });

        doIfHover(element, function () {
            element.mouseInside = false;

            element.addEventListener('mouseenter', function () {
                element.mouseInside = true;
                element.message({
                    type: 'mouseenter'
                });
            });

            element.addEventListener('mouseleave', function () {
                element.mouseInside = false;
                element.message({
                    type: 'mouseleave'
                });
            });
        });
    },

    attached: function (element) {
        enforce(element).attributeExists('id');
    },

    detached: function (element) {
        if (element._auiAlignment) {
            element._auiAlignment.destroy();
        }
    },

    template: function (element) {
        $(element)
            .addClass('aui-layer')
            .attr('aria-hidden', 'true')
            .html('<div class="aui-inline-dialog-contents">' + element.innerHTML + '</div>');
    }
});

amdify('aui/inline-dialog2', InlineDialog2);
globalize('InlineDialog2', InlineDialog2);
export default InlineDialog2;
