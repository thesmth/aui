'use strict';

import $ from './jquery';
import * as deprecate from './internal/deprecation';
import globalize from './internal/globalize';

/**
 * Does nothing because legacy code.
 *
 * @returns {undefined}
 */
function warnAboutFirebug () {}

/**
 * Includes firebug lite for debugging in IE. Especially in IE.
 *
 * @returns {undefined}
 */
function firebug () {
    var script = $(document.createElement('script'));

    script.attr('src', 'https://getfirebug.com/releases/lite/1.2/firebug-lite-compressed.js');
    $('head').append(script);

    (function () {
        if (window.firebug) {
            firebug.init();
        } else {
            setTimeout(firebug, 0);
        }
    })();
}

firebug = deprecate.fn(firebug, 'firebug', {
    sinceVersion: '5.1.0'
});

warnAboutFirebug = deprecate.fn(warnAboutFirebug, 'warnAboutFirebug', {
    sinceVersion: '5.8.0'
});

globalize('firebug', firebug);
globalize('warnAboutFirebug', warnAboutFirebug);

export {
    firebug,
    warnAboutFirebug
};
