'use strict';

import skate from 'skatejs';
import skateTemplateHtml from 'skatejs-template-html';
import enforce from './internal/enforcer';

function getLabel(element) {
    return element.querySelector('label');
}

function updateLabelFor(element, change) {
    if (element.hasAttribute('for')) {
        getLabel(element).setAttribute('for', `${change.newValue}-input`);
    } else {
        getLabel(element).removeAttribute('for');
    }
}

function updateLabelForm(element, change) {
    if (element.hasAttribute('form')) {
        getLabel(element).setAttribute('form', change.newValue);
    } else {
        getLabel(element).removeAttribute('form');
    }
}

var Label = skate('aui-label', {
    template: skateTemplateHtml('<label><content></content></label>'),
    created: function (element) {
        element._label = getLabel(element);    // required for quick access from test
    },
    attached: function (element) {
        enforce(element).attributeExists('for');
    },
    attributes: {
        'for': updateLabelFor,
        'form':  updateLabelForm
    }
});

export default Label;
