# 5.9.0 [Unreleased]

[Documentation](https://docs.atlassian.com/aui/5.9.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.9.0)

## Added
* Form validation now has an API method to query the validation status of a whole form.
* `<aui-select>` has a new option that allows the user to create new values.
* `<aui-select>` has a new option that restricts the select to not allow empty choices.
* Dropdown2 now has a web component api `<aui-dropdown>`
* New toggle button web component `<aui-toggle>`.
* New AUI label web component `<aui-label>`, for use with other AUI components.
* AUI Badge now has a web component api `<aui-badge>`.

## Changed
* Moved docs and test pages from AUI-ADG to AUI.
* Refactored build from grunt to gulp.
* Experimental components no longer need a `require()` call to load them.
* `<aui-inline-dialog2>` renamed to `<aui-inline-dialog>`.
* `<aui-inline-dialog>` no longer needs a `require()` call to load it.

## Deprecated
* `AJS()`, `AJS.filterBySearch()`, `AJS.include()`, `AJS.setVisible()`, `AJS.setCurrent()`, `AJS.isVisible()`. Use jQuery or native alternatives instead.
* Restful table helpers: `AJS.triggerEvt()`, `AJS.bindEvt()`, `AJS.triggerEvtForInst()`. These methods have been moved inside restful table.
* jQuery element `$el` in form validation field. Use the native element `el` instead.
* Usages of the class `.aui-badge` has been deprecated. Use the `<aui-badge>` web component instead.

## Fixed
* Several theming fixes and enhancements.

# 5.8.13
* [Documentation](https://docs.atlassian.com/aui/5.8.13/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.13)

## Added
* Added `debounceImmediate()` (similar to `debounce()` but is immediately invoked).

## Changed
* Backport of jQuery UI feature detection performance improvement.
* "More" menu for responsive header is now lazily created to reduce work in onReady.

## Fixed
* `AJS.version` now contains the correct value rather than `${project.version}` (broken since 5.8.11).
* Various bug fixes for responsive header (reinsertion order, disappearing items, quickly resizing, keyboard navigation).
* Fixed a JavaScript error in the responsive header when secondary navigation was hidden.
* Fixed navigation soy template when there are collapsible children.
* Fixed keyboard navigation bug in Sidebar around collapsible children.

# 5.8.12

* [Documentation](https://docs.atlassian.com/aui/5.8.12/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.12)

## Changed
* Clicking on empty space in sidebar no longer collapses or expands it.

## Fixed
* Improved robustness of Raphael detection.
* Sidebar in documents with short content now properly reflows when expanding window size in Chrome.
* Sidebar submenus can now be focused with the keyboard when collapsed.
* Keyboard shortcut ("[") for toggling the sidebar now works on German keyboard layouts.
* Fixed a sidebar JavaScript bug in IE9 (invalid classList lookup).
* Fixed a sidebar bug in IE9/10 where submenus would incorrectly show when hovered over.

# 5.8.11

* [Documentation](https://docs.atlassian.com/aui/5.8.11/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.11)

## Changed
* dropdown2's `.aui-dropdown2-checkbox` and `.aui-dropdown2-radio` have had their `isDisabled()` method replaced with `isEnabled()`.

## Fixed
* Responsive header dropdown2s now properly open as submenus.
* Hidden dropdown2s no longer break alignment of menus on window resize.

# 5.8.10

* [Documentation](https://docs.atlassian.com/aui/5.8.10/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.10)

## Fixed
* `eve.unbind()` is once again aliased as `eve.off()`.

# 5.8.9

* [Documentation](https://docs.atlassian.com/aui/5.8.9/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.9)

## Fixed
* Proper fix for messages wrapping long unbroken strings.

# 5.8.8

* [Documentation](https://docs.atlassian.com/aui/5.8.8/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.8)

## Fixed
* Form validation attributes changed at runtime are now respected on subsequent field validations.
* Messages now wrap long unbroken strings (break-word).
* Links (<a>) in the expanded sidebar now work again (broken since 5.8.0).
* Sidebar correctly sizes and positions itself at page load for short content heights.

# 5.8.7

* [Documentation](https://docs.atlassian.com/aui/5.8.7/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.7)

## Highlights
* Sidebar events now support `preventDefault()`.

# 5.8.6

* [Documentation](https://docs.atlassian.com/aui/5.8.6/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.6)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.5

* [Documentation](https://docs.atlassian.com/aui/5.8.5/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.5)

## Upgrade Notes
* This release is broken due to cross-branch contamination. Please use 5.8.7 instead.

# 5.8.4

* [Documentation](https://docs.atlassian.com/aui/5.8.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.4)

# 5.8.3

* [Documentation](https://docs.atlassian.com/aui/5.8.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.3)

# 5.8.2

* [Documentation](https://docs.atlassian.com/aui/5.8.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.2)

## Upgrade Notes
* This release has some missing artifacts, please use 5.8.3 instead.

# 5.8.1

* [Documentation](https://docs.atlassian.com/aui/5.8.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.1)

# 5.8.0

[Documentation](https://docs.atlassian.com/aui/5.8.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.8.0)

## Highlights
* NEW COMPONENT: [AUI Single Select](http://docs.atlassian.com/aui/5.8.0/single-select.html)
* Dropdown2 has been rewritten to improve accessibility. There is a new markup pattern, detailed at [http://docs.atlassian.com/aui/5.8.0/dropdown2.html](http://docs.atlassian.com/aui/5.8.0/dropdown2.html). While the old markup pattern will work backwards compatibly, we encourage all developers to move to the new markup pattern to improve access for people using screen readers.

## Upgrade Notes
* aui-ie9.css has been removed from the flatpack. It is no longer necessary to include this file.
* If you are using the sidebar from the flatpack, you will now need to include aui-experimental.js
* The contents of an AUI Dropdown2 can now be created entirely using Soy (instead of sending html to aui.dropdown2.contents)
* The markup generated using the AUI Dropdown2 Soy templates has changed. This new markup pattern is now much more accessible to screen readers.
* Removed dependencies from AUI components to AUI soy templates (including responsive header).
* AUI Sandbox has been removed, in the future we will be uploading examples to jsbin.
* AUI Flag `persistent` option has been removed (deprecated in 5.7.7).  Update all usages of this option to use `close` instead (see http://docs.atlassian.com/aui/5.8.0/flag.html).
* An AMD loader must be included in order to use aui-experimental.js now.

# 5.7.20
[Documentation](https://docs.atlassian.com/aui/5.7.20/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.20)

# 5.7.19
[Documentation](https://docs.atlassian.com/aui/5.7.19/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.19)

# 5.7.18
[Documentation](https://docs.atlassian.com/aui/5.7.18/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.18)

# 5.7.17
[Documentation](https://docs.atlassian.com/aui/5.7.17/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.17)

# 5.7.16
[Documentation](https://docs.atlassian.com/aui/5.7.16/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.16)

## Highlights
* The dist is now using version 0.11.4 of grunt-less-contrib, and version ~1.7.2 of the less compiler.

# 5.7.15
[Documentation](https://docs.atlassian.com/aui/5.7.15/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.15)

# 5.7.14
[Documentation](https://docs.atlassian.com/aui/5.7.14/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.14)

# 5.7.13
[Documentation](https://docs.atlassian.com/aui/5.7.13/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.13)

# 5.7.12
[Documentation](https://docs.atlassian.com/aui/5.7.12/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.12)

# 5.7.11
[Documentation](https://docs.atlassian.com/aui/5.7.11/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.11)

# 5.7.10
[Documentation](https://docs.atlassian.com/aui/5.7.10/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.10)

# 5.7.9
[Documentation](https://docs.atlassian.com/aui/5.7.9/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.9)

# 5.7.8
[Documentation](https://docs.atlassian.com/aui/5.7.8/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.8)

# 5.7.7
[Documentation](https://docs.atlassian.com/aui/5.7.7/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.7)

## Highlights
* Fixed persistent flags to be dismissable. See [AUI-2893](https://ecosystem.atlassian.net/browse/AUI-2893)

## Upgrade Notes
**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Flags created with the previous 5.7.1 API will work exactly as they did previously in 5.7.7. Upgrading from these versions
is **non breaking** (existing calls to flag will behave exactly as they did). However, the "persistent" has been deprecated,
and the "close" option introduced instead.
* Flags with "persistent: true" should become "close: never"
* Flags with "persistent: false" should become "close: manual" (close: "manual" is the default, so you do not need to explicitly set this)

**Upgrading from 5.7.0, 5.7.1, 5.7.2, 5.7.3**
Upgrading from 5.7.4 **will change** behaviour of flags with "persistent: false" set. In 5.7.4, flags automatically faded
from view. They now require a user to dismiss them (they have close: "manual" behaviour)
* Flags with "persistent: true" should become "close: never". Flags with "persistent: true" maintain the same behaviour.
* Flags with "persistent: false" no longer fade after five seconds. If you wish them to, they should become "close: auto". If you do do not wish them to, they should become "close: manual"

#5.7.6
This is a broken release containing no bugfixes and should not be used. Please use the latest 5.7.x release instead

#5.7.5
[Documentation](https://docs.atlassian.com/aui/5.7.5/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.5)

#5.7.4
[Documentation](https://docs.atlassian.com/aui/5.7.4/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.4)

## Highlights
* Flags auto-close was ported over to this release from master.

#5.7.3
[Documentation](https://docs.atlassian.com/aui/5.7.3/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.3)

## Highlights
* New Confluence Icon Fonts
Note: normally we would do this in a minor version. However to avoid upgrade friction for Confluence we've determined it is acceptable to put these in a patch release.

#5.7.2
[Documentation](https://docs.atlassian.com/aui/5.7.2/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.2)

#5.7.1
[Documentation](https://docs.atlassian.com/aui/5.7.1/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.1)

#5.7.0
[Documentation](https://docs.atlassian.com/aui/5.7.0/) •
[JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=5.7.0)

## Highlights
* Buttons have become flat and have had gradients removed
* Inset shadows have been removed from fields in forms
* Form notification is available as a new API (show tooltip messages on fields)
* Flag messages are available as a new API
* Inline Dialog 2 is available as a new API
* System notifications have been changed (no longer electric Charlie)
* The following components have no global variable and are only exposed via AMD modules:
  * Flags
  * Form Notification
  * Form Validation
  * Inline Dialog 2
* [Animated examples](https://developer.atlassian.com/display/AUI/AUI+5.7.0+Release+Notes) are available.

## Upgrade Notes
* If you have been using Form Validation, please change your data attributes
  * from: `<input data-aui-validate-... />`
  * to: `<input data-aui-validation-... />`
* If you are using Inline Dialog 2, Flags, Form Notification or Form Validation, you will need an AMD loader to initialise their behaviour on pages:
  * `require(['aui/inline-dialog2']); // Initialises all Inline Dialog 2s`
  * `require(['aui/flag']); // Initialises all flags`
  * `require(['aui/form-notification']); // Initialises all form notifications`
  * `require(['aui/form-validation', 'form-provided-validators']); // Loads and initialises form validations (and notifications) API. Also loads all AUI-provided form validators`

For all release notes older than 5.7.0, check [DAC](https://developer.atlassian.com/display/AUI/AUI+release+notes)
