//This file exists so you can run karma.start from root, gulp does not use this file

var args = require('./build/gulp/args');

module.exports = function(karma) {
    var config = require('./build/configs/karma-shared')(args);
    config.singleRun = false;
    config.autoWatch = true;
    karma.set(config);
};
