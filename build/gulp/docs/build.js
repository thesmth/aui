'use strict';

var docsAssets = require('./assets');
var docsClean = require('./clean');
var docsJs = require('./js');
var docsLess = require('./less');
var docsLessAssets = require('./less-assets');
var docsMetalsmith = require('./metalsmith');
var mac = require('mac');

module.exports = mac.series(
    docsClean,
    docsLessAssets,
    docsAssets,
    docsMetalsmith,
    mac.parallel(
        docsJs,
        docsLess
    )
);
