'use strict';

var gulp = require('gulp');

module.exports = function () {
    var opts = this || {};
    var paths = opts.paths || 'src/less/{fonts,images}/**/*';
    return gulp
        .src(paths)
        .pipe(gulp.dest('.tmp/docs/src/styles'));
};
