'use strict';

var commander = require('../../lib/commander');
var galvatron = require('../../lib/galvatron');
var gulp = require('gulp');
var mac = require('mac');
var minify = require('../../lib/minify');

module.exports = mac.mac({
    on: 'data end'
}).series(
    function () {
        var bundle = galvatron.bundle('docs/src/scripts/index.js');
        return gulp.src(bundle.files)
            .pipe(bundle.watchIf(commander.watch))
            .pipe(bundle.stream())
            .pipe(minify())
            .pipe(gulp.dest('.tmp/docs/dist/scripts'));
    }
);
