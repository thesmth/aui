'use strict';
var commander = require('../../lib/commander');
var del = require('del');
var docsServer = require('./server');
var mac = require('mac');
var metalsmith = require('metalsmith');
var metalsmithLayouts = require('metalsmith-layouts');
var metalsmithMarkdown = require('metalsmith-markdown');
var metalsmithTextReplace = require('metalsmith-text-replace');
var metalsmithWatch = require('metalsmith-watch');
var pkg = require('../../../package.json');

commander.option('--localdist', 'If set, the push to CDN feature will use a local dist');

module.exports = mac.series(
    function (done) {
        var localDistLocation = '//' + docsServer.getHost() + ':' + docsServer.getPort() + '//aui/';
        var ms = metalsmith('.tmp/docs')
            .destination('./dist')
            .use(metalsmithMarkdown())
            .use(metalsmithLayouts({
                directory: 'layouts',
                engine: 'handlebars'
            }))
            .use(metalsmithTextReplace({
                '**/**/*.html': [{
                    find: /\$\{project\.version\}/g,
                    replace: pkg.version
                }, {
                    find: /\$\{dist\.location\}/g,
                    replace: commander.localdist ? localDistLocation : ('//aui-cdn.atlassian.com/aui-adg/' + pkg.version)
                }]
            }));

        if (commander.watch) {
            ms.use(metalsmithWatch());
        }

        ms.build(function (err) {
            if (err) { throw err; }
            done();
        });
    },

    // Cleanup unneeded items copied by Metalsmith.
    function (done) {
        del([
            '.tmp/docs/dist/scripts/*',
            '.tmp/docs/dist/styles/*.less'
        ], done);
    }
);
