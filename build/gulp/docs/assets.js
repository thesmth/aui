'use strict';

var commander = require('../../lib/commander');
var gulp = require('gulp');
var gulpWatch = require('gulp-watch');
var path = require('path');

module.exports = function () {
    var opts = this || {};
    var paths = opts.paths || 'docs/**/*';

    if (commander.watch) {
        gulpWatch(paths, function (e) {
            gulp.src(e.path).pipe(gulp.dest(function (file) {
                var rel = path.relative(process.cwd(), file.path);
                return path.join('.tmp', path.dirname(rel));
            }));
        });
    }

    return gulp.src(paths).pipe(gulp.dest('.tmp/docs'));
};
