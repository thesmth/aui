'use strict';

var commander = require('../../lib/commander');
var gulp = require('gulp');
var gulpLess = require('gulp-less');
var gulpWatchLess = require('gulp-watch-less');
var mac = require('mac');
var path = require('path');
var streamIf = require('../../lib/stream-if');
var src = 'docs/src/styles/index.less';

module.exports = mac.mac({
    on: 'data end'
}).series(
    function () {
        return gulp.src(src)
            .pipe(streamIf(commander.watch && gulpWatchLess(src)))
            .pipe(gulpLess({ paths: [ path.basename(src) ] }))
            .pipe(gulp.dest('.tmp/docs/dist/styles'));
    }
);
