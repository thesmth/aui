var mac = require('../../lib/mac');
var copyAndWatch = require('../../lib/copy-and-watch');

module.exports = mac.parallel(
    copyAndWatch('dist/aui/**', '.tmp/flatapp/src/static/common'),
    copyAndWatch('tests/flatapp/src/soy/**/*.soy', '.tmp/flatapp/src/soy'),
    copyAndWatch('tests/flatapp/src/static/**', '.tmp/flatapp/src/static'),
    copyAndWatch('tests/test-pages/pages/**/*.soy', '.tmp/flatapp/src/soy/pages'),
    copyAndWatch(['tests/test-pages/pages/**', '!**/*.soy'], '.tmp/flatapp/src/static/pages'),
    copyAndWatch('tests/test-pages/common/**/*.soy', '.tmp/flatapp/src/soy/dependencies'),
    copyAndWatch(['tests/test-pages/common/**.css', 'tests/test-pages/common/**.js'], '.tmp/flatapp/src/static/common')
);
