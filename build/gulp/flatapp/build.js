'use strict';

var log = require('../../lib/log');
var path = require('path');
var pkg = require('../../../package.json');
var spawn = require('child_process').spawn;
var rootPath = require('../../lib/root-path');
var mac = require('../../lib/mac');
var copyAndWatch = require('../../lib/copy-and-watch');
var gulp = require('gulp');
var gulpBatch = require('gulp-batch');

function soyRender (cb) {
    var opts = {
        baseDir: path.join('.tmp', 'flatapp', 'src', 'soy', 'pages'),
        i18nBundle: '.tmp/i18n.properties',
        glob: '**.soy',
        outDir: path.join('.tmp', 'flatapp', 'target', 'static', 'pages'),
        rootNamespace: 'testPages.pages',
        data: {
            auiVersion: pkg.version,
            language: 'en'
        },
        dependencies: [{
            // AUI soy templates
            baseDir: path.join(rootPath, 'src/soy'),
            glob: '**.soy'
        }, {
            // flatapp dependencies
            baseDir: path.join('.tmp', 'flatapp', 'src', 'soy', 'dependencies'),
            glob: '**.soy'
        }]
    };

    var params = [
        '-jar', './build/jar/atlassian-soy-cli-support-2.4.0-SNAPSHOT-jar-with-dependencies.jar',
        '--type', 'render',
        '--i18n', opts.i18nBundle,
        '--basedir', opts.baseDir,
        '--glob', opts.glob,
        '--outdir', opts.outDir
    ];

    if (opts.outputExtension) {
        params = params.concat(['--extension', opts.outputExtension]);
    }

    if (opts.rootNamespace) {
        params = params.concat(['--rootnamespace', opts.rootNamespace]);
    }

    if (opts.dependencies) {
        var depString = opts.dependencies.map(function(dep) {
            return dep.baseDir + ':' + dep.glob;
        }).join(',');
        params = params.concat(['--dependencies', depString]);
    }

    if (opts.data) {
        var dataString = Object.keys(opts.data).map(function(key) {
            return key + ':' + opts.data[key];
        }).join(',');
        params = params.concat(['--data', dataString]);
    }

    var javaExec = process.env.JAVA_HOME ? process.env.JAVA_HOME + '/bin/java' : 'java';
    var cmd = spawn(javaExec, params, { stdio: 'inherit' });

    cmd.on('close', function(code) {
        if (code === 0) {
            cb();
        } else {
            cb(code);
        }
    });
}

var compileSoy = function (done) {
    log.info('Compiling soy');

    soyRender(function (err) {
        log.info('Flatapp soy rendered');
        done(err);
    });

    gulp.watch('.tmp/flatapp/target/soy/**', gulpBatch(function(events, cb) {
        events.on('data', console.log).on('end', function (event) {
            soyRender(function (err) {
                log.info('Flatapp soy rendered');
                cb(err);
            });
        });
    }));
};

module.exports =  mac.series(
    copyAndWatch('.tmp/flatapp/src/**/*', '.tmp/flatapp/target'),
    compileSoy
);
