'use strict';

var del = require('del');

module.exports = function () {
    del([
        '.tmp',
        'dist'
    ]);
};
