'use strict';
var mac = require('../lib/mac');
var flatappServer = require('./flatapp/server');
var flatappCopy = require('./flatapp/copy');
var flatappBuild = require('./flatapp/build');
var dist = require('./dist');
var i18n = require('./i18n');

module.exports = mac.series(
    dist,
    i18n,
    flatappCopy,
    flatappBuild,
    flatappServer
);
