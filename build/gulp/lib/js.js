'use strict';

var gulp = require('gulp');
var babel = require('gulp-babel');
var filter = require('gulp-filter');
var sourcemaps = require('gulp-sourcemaps');
var vinylTransform = require('vinyl-transform');
var mapStream = require('map-stream');
var versionReplace = require('../../lib/version-replace');

var gulpVersionReplace = vinylTransform(function () {
    return mapStream(function (data, next) {
        return next(null, versionReplace(data));
    });
});

var babelOptions = {
    modules: 'umd'
};

module.exports = function () {
    var toBabel = filter(['js/aui/**', 'js/aui*.js']);

    return gulp.src(['./src/{js,js-vendor}/**', '!./src/js/aui-soy.js'])
        .pipe(gulpVersionReplace)
        .pipe(toBabel)
        .pipe(sourcemaps.init())
        .pipe(babel(babelOptions))
        .pipe(sourcemaps.write('.'))
        .pipe(toBabel.restore())
        .pipe(gulp.dest('./lib'));
};
