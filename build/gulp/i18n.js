'use strict';

var fs = require('fs-extra');

module.exports = function (done) {
    var keys = require('../../lib/js/aui/internal/i18n/keys');
    var props = '';

    for (var key in keys) {
        if (Object.prototype.hasOwnProperty.call(keys, key)) {
            props += key + ' = ' + keys[key] + '\n';
        }
    }

    fs.outputFile('.tmp/i18n.properties', props, done);
};
