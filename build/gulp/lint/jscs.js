'use strict';

var gulp = require('gulp');
var gulpJscs = require('gulp-jscs');

module.exports = function() {
    return gulp.src([
            'gulpfile.js',
            'src/js/**.js'
        ])
        .pipe(gulpJscs());
};
