'use strict';

var fs = require('fs-extra');
var gulp = require('gulp');
var gulpMinifyCss = require('gulp-minify-css');
var gulpRename = require('gulp-rename');
var less = require('../../lib/less');
var mac = require('../../lib/mac');

function cleanup () {
    fs.removeSync('dist/aui/css');
    fs.mkdirsSync('dist/aui/css');
}

function mainImages () {
    return gulp.src('src/less/images/**/*')
        .pipe(gulp.dest('dist/aui/css/images'));
}

function select2Images () {
    return gulp.src([
        'src/css-vendor/jquery/plugins/*.png',
        'src/css-vendor/jquery/plugins/*.gif'
    ])
        .pipe(gulp.dest('dist/aui/css'));
}

function lessFiles () {
    var files = 'src/less/batch/*.less';
    return gulp.src(files)
        .pipe(less(files))
        .pipe(gulp.dest('dist/aui/css'))
        .pipe(gulpMinifyCss())
        .pipe(gulpRename({ suffix: '.min' }))
        .pipe(gulp.dest('dist/aui/css'));
}

module.exports = mac.series(
    cleanup,
    mac.parallel(
        mainImages,
        select2Images,
        lessFiles
    )
);
