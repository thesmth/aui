'use strict';

var mac = require('../lib/mac');
var distJs = require('./dist/js');
var distLess = require('./dist/less');

module.exports = mac.parallel(
    distJs,
    distLess
);
