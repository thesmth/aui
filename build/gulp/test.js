'use strict';

var commander = require('../lib/commander');
var galvatron = require('../lib/galvatron');
var gulp = require('gulp');
var karma = require('karma').server;
var log = require('../lib/log');
var mac = require('../lib/mac');

commander
    .option('-b, --browsers [Chrome_1024x768]', 'Browsers to run tests against')
    .option('-c, --coverage', 'Enable code coverage reporting.')
    .option('-g, --grep [pattern]', 'The grep pattern matching the tests you want to run.')
    .parse(process.argv);

var browsers = (commander.browsers || 'Chrome_1024x768,Firefox').split(',');
var clientArgs = [];
var reporters = ['progress', 'junit'];

if (commander.coverage) {
    reporters.push('coverage');
}

if (commander.grep) {
    clientArgs.push('--grep');
    clientArgs.push(commander.grep);
}

function run () {
    log.info('Running tests...');
    karma.start({
        hostname: commander.watch ? '0.0.0.0' : 'localhost',
        autoWatch: !!commander.watch,
        singleRun: !commander.watch,
        frameworks: ['mocha', 'sinon-chai'],
        browsers: browsers,
        customLaunchers: {
            Chrome_1024x768 :{
                base: 'Chrome',
                flags: ['--window-size=1024,768']
            }
        },
        reporters: reporters,
        client: {
            args: clientArgs
        },
        coverageReporter: {
            dir: 'reports/istanbul',
            type: 'html'
        },
        junitReporter: {
            outputFile: 'tests/karma.xml',
            suite: ''
        },
        preprocessors: {
            'src/less/{**/*,*}.less': 'less',
        },
        lessPreprocessor: {
            options: {
                paths: ['src/less/**'],
                save: false,
                relativeUrls: true
            }
        },
        files: [
            'src/less/batch/aui-experimental.less',
            'src/less/batch/aui.less',
            'tests/styles/all.css',

            // Depending on which jQuery version has been installed by Bower the
            // dist file might be in a different location. Since we should only
            // have one version of jQuery installed at a given time, only one
            // of these paths should resolve.
            'bower_components/jquery/jquery.js',
            'bower_components/jquery/dist/jquery.js',

            'bower_components/jquery-migrate/jquery-migrate.js',
            '.tmp/unit.js'
        ]
    });
}

module.exports = mac.series(
    function () {
        var bundle = galvatron.bundle('tests/unit.js');
        return gulp.src(bundle.files)
            .pipe(bundle.watchIf(commander.watch))
            .pipe(bundle.stream())
            .pipe(gulp.dest('.tmp'));
    },
    run
);
