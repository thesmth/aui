'use strict';

var babel = require('babel');
var galvatron = require('galvatron');
var log = require('./log');
var minimatch = require('minimatch');
var path = require('path');
var versionReplace = require('./version-replace');

var galv = galvatron();
var out = process.stdout;
var root = path.join(__dirname, '..', '..');
var supportsExtendedStdoutMethods = !!out.clearLine;

function relative (file) {
    return path.relative(process.cwd(), file);
}

galv.events
    .on('compile', function (file, index, traced, bundle) {
        var dest = bundle.map(relative).join(', ');
        var num = index + 1;
        var total = traced.length;
        var percent = Math.round((num / total) * 100);

        if (supportsExtendedStdoutMethods) {
            out.clearLine();
            out.cursorTo(0);
        }

        // This will print out once for every line if extended stdout methods
        // are not supported. Should work fine in Mac.
        out.write('Compiling ' + dest + ': ' + num + ' of ' + total + ' files (' + percent + '%)');

        if (!supportsExtendedStdoutMethods || index === traced.length -1) {
            out.write('\n');
        }
    })
    .on('error', function (error, file) {
        if (file) {
            log.error('ERROR %s: %s', relative(file), error);
        } else {
            log.error('ERROR %s', error);
        }
    })
    .on('update', function (file, main) {
        log.info('UPDATE %s -> %s', relative(file), relative(main));
    });

galv.fs.map({
    // This is required because Backbone imports "jquery" and that will import
    // the entire jQuery source if we don't tell it to use our shim instead.
    jquery: path.join(root, 'src', 'js', 'aui', 'jquery.js'),

    // Sandy uses Browserify which leaves "require()" calls intact and these
    // reference relative paths. Galvatron can't find these when tracing its
    // source unless we map all of them. Let's just use the ES6 source directly.
    sandy: path.join('bower_components', 'sandy', 'src', 'sandy.js'),

    // Once we don't have modified sources we can remove this to resolve them
    // from our node_modules. For now, this fixes the issue when we require
    // Backbone it tries to require Underscore using its module name rather
    // than it's path. This would then normally look in the node_modules
    // directory, but the map forces it to resolve to a path.
    underscore: path.join(root, 'src', 'js-vendor', 'underscorejs', 'underscore.js')
});

galv.transformer
    .post(function (code, data) {
        var isJsVendor = minimatch(data.path, '**/js-vendor/**/*');
        if (isJsVendor) {
            return code;
        }
        var transformedCode = babel.transform(code, { blacklist: ['useStrict'] }).code;
        return transformedCode;
    })
    .post(versionReplace)
    .post('globalize');

module.exports = galv;
