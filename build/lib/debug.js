'use strict';

var util = require('util');

function defaultTemplate (message) {
    var date = new Date();
    return util.format(
        '[%s] %s',
        date.toTimeString().split(' ')[0],
        message
    );
}

module.exports = function (levels) {
    if (!Array.isArray(levels)) {
        levels = (levels || '').split(',');
    }

    return function (level, options) {
        var shouldLog;

        shouldLog = levels.indexOf(level) > -1;
        options = options || {
            template: defaultTemplate,
            transports: [console.log.bind(console)]
        };

        return function (message) {
            if (!shouldLog) {
                return;
            }

            message = message || '';
            message = options.template(util.format.apply(null, [message].concat([].slice.call(arguments, 1))));

            options.transports.forEach(function (transport) {
                transport(message);
            });
        };
    };
};
