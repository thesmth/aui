'use strict';

var through = require('through');

module.exports = function (condition, callback) {
    return callback && callback(data) || condition || through();
};
