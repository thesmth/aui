var cc = require('closure-compiler');
var vinylTransform = require('vinyl-transform');
var mapStream = require('map-stream');

module.exports = function () {
    return vinylTransform(function () {
        return mapStream(function (data, next) {
            cc.compile(data, {}, function afterCompile (err, stdout) {
                if (err) {
                    return next(err)
                } else {
                    return next(null, stdout);
                }
            });
        });
    });
}
