#!/bin/bash
set -x
set -e

BRANCH=$1
RELEASE_VERSION=$2
NEXT_VERSION=$3
if [ -z "${BRANCH}" -o -z "${RELEASE_VERSION}" -o -z "${NEXT_VERSION}" ]; then
  echo "Usage: $(basename ${0}) [branch] [release-version] [next-version]"
  exit 1
fi

basedir=$(pwd)

##
# Tag aui with the new version and publish the tag.
#
cd ${basedir}/aui
npm version ${RELEASE_VERSION}
# Rename the tag NPM creates (since we don't want the "v" prefix).
git tag -d v${RELEASE_VERSION}
git tag -a ${RELEASE_VERSION} -m "Release ${RELEASE_VERSION}."
git push git@bitbucket.org:atlassian/aui.git ${RELEASE_VERSION}

##
# Update dist repo.
#
npm run dist

cd ${basedir}
git clone git@bitbucket.org:atlassian/aui-dist
rm -rf ${basedir}/aui-dist/*
rm -rf ${basedir}/aui-dist/.gitignore

cd ${basedir}/aui-dist
cp -rf ${basedir}/aui/dist/* .
git add .
git commit -am "Release ${RELEASE_VERSION}."
git tag -a ${RELEASE_VERSION} -m "${RELEASE_VERSION}"
git push origin ${RELEASE_VERSION}

cd ${basedir}
rm -rf ${basedir}/aui-dist

##
# Bumps the version in all modules and pushes.
#
cd ${basedir}/aui
git reset --hard HEAD^  # Prior to the "npm version" auto-generated commit.

ret=0
npm version ${NEXT_VERSION} || ret=$?
if [ ${ret} -eq 0 ]; then
  git tag -d v${NEXT_VERSION}
  git push git@bitbucket.org:atlassian/aui.git ${BRANCH}
fi
