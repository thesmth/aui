'use strict';

var glob = require('glob');
var sh = require('shelljs');
var soy = require('../lib/soy');
var wrap = require('../lib/wrap-file');

// Fix so that soy code exports properly to the window when wrapped in an iife.
var wrapAui = wrap('(function(){\naui = window.aui = window.aui || {}\n', '\n}());');
var wrapAtl = wrap('(function(){\n', '\n}());');
var wrapGoog = wrap('(function(){\ngoog = window.goog = window.goog || {};\n', '\nwindow.soy = soy;\n}());');

sh.rm('-rf', '.tmp/compiled-soy');

soy({
    args: {
        basedir: 'src/soy',
        glob: '**.soy',
        i18n: 'src/i18n/aui.properties',
        outdir: '.tmp/compiled-soy'
    }
});

// This glob must be synchronous otherwise the below files (atlassian-deps.js
// and soyutils.js) may also get the aui wrapper (DP-495).
glob.sync('.tmp/compiled-soy/*.js').forEach(function (file) {
    wrapAui(file);
});

wrapAtl('bower_components/soyutils/js/atlassian-deps.js', '.tmp/compiled-soy/atlassian-deps.js');
wrapGoog('bower_components/soyutils/js/soyutils.js', '.tmp/compiled-soy/soyutils.js');
