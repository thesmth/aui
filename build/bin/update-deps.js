var helper = require('./bin-helper');

var bowerArgs = ['install', '--allow-root'];
var term = process.env['TERM'];
if (!term || term === 'dumb') {
    bowerArgs.push('--no-color');
}

helper.chain([
    [helper.npmNormalize('./node_modules/.bin/bower'), bowerArgs],
    ['jar', ['xf', 'index.jar'], {
        cwd: helper.pathNormalize('./bower_components/soyutils')
    }],
    ['node', ['./build/bin/soy']]
]);
